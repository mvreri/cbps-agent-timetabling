package ttgui;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author MURERI
 */
public class cbpsGui extends JFrame implements ActionListener, ItemListener {
        public ttagents.agentcbps myAgent;
	
	public JTextField schoolField, deptField, courseField, semField, yearField;
        public JComboBox jc1,jc2,jc3,jc4;
        
        
        private Connection conn = null;
        private Statement stmt = null;
        private ResultSet rs = null;    
        private String dbURL = "jdbc:mysql://localhost/timetabling";
        private String username = "root";
        private String password = "";
        private String sname, sid,did,couid,cname;
        private int schid, cid;
        private Hashtable<Object, Object> subItems = new Hashtable<Object, Object>();

        
        
        
        
        
        
	
	public cbpsGui(ttagents.agentcbps a) {
		super(a.getLocalName());
		
		myAgent = a;
                
                JPanel p = new JPanel();
		p.setLayout(new GridLayout(5, 2));               
                
                String[] items = {"Select School/Department", "School of Biological Sciences", "School of Mathematics", "School of Computing and Informatics",
                    "Department of Chemistry", "Department of Geology", "Department of Meteorology", "Department of Physics"};
                
                p.add(new JLabel("School / Department "));
                jc1 = new JComboBox(items);
                p.add(jc1);
                jc1.addActionListener(this);
                jc1.addItemListener(this);
                
                p.add(new JLabel("Course"));
                jc2 = new JComboBox();
                jc2.setPrototypeDisplayValue("XXXXXXXXXX");
                p.add(jc2);
                jc2.addItemListener(this);
                
                String[] subItems1 = {"Course", "BSc. Environmental Conservation and Natural Resource Management", "BSc. Biology", "BSc. Microbiology and Biotechnology, BSc. General"};
                subItems.put(items[1], subItems1);
                String[] subItems2 = {"Course", "BSc. Actuarial Science", "BSc. Mathematics", "BSc. Statistics"};
                subItems.put(items[2], subItems2);
                String[] subItems3 = {"Course", "BSc. Computer Science"};
                subItems.put(items[3], subItems3);
                String[] subItems4 = {"Course","BSc. Environmental Chemistry", "BSc. Analytical Chemistry", "BSc. Industrial Chemistry",
                    "BSc. Chemistry" };
                subItems.put(items[4], subItems4);
                
                String[] subItems5 = {"Course", "BSc. Geology", "BSc. Environmental Geoscience"};
                subItems.put(items[5], subItems5);
                
                String[] subItems6 = {"Course", "BSc. Meteorology", "BSc. Atmospheric Science" };
                subItems.put(items[6], subItems6);
                String[] subItems7 = {"Course","BSc. Microprocessor Technology and Instrumentation",
                "BSc. Physics", "BSc. Astronomy and Astrophysics", "BEd. Science (Physics)"};
                subItems.put(items[7], subItems7);
                
                
                String[] Semester = {"One", "Two"};
                p.add(new JLabel("Semester"));
		jc3 = new JComboBox(Semester);
		p.add(jc3);
                
                String[] level = {"1","2","3", "4"};
                p.add(new JLabel("Group Year (Level)"));
                jc4 = new JComboBox(level);
                p.add(jc4);
                
                
                
                
                
                getContentPane().add(p, BorderLayout.CENTER);
                //add actions to the interface components
		JButton addButton = new JButton("Add");
		addButton.addActionListener( new ActionListener() {
			public void actionPerformed(ActionEvent ev) {
				try {
					String schName =jc1.getModel().getSelectedItem().toString();
                                        String courseName = jc2.getModel().getSelectedItem().toString();
                                        String sem =jc3.getModel().getSelectedItem().toString();
					String year =jc4.getModel().getSelectedItem().toString();
                                        
					myAgent.lookupData(schName, courseName, sem, Integer.parseInt(year));
					
                                        
				}
				catch (Exception e) {
					JOptionPane.showMessageDialog(cbpsGui.this, "Invalid values. "+e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE); 
				}
			}
		} );
                
                            
                
                
		p = new JPanel();
		p.add(addButton);
		getContentPane().add(p, BorderLayout.SOUTH);
		
		// Make the agent terminate when the user closes 
		// the GUI using the button on the upper right corner	
		addWindowListener(new	WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				myAgent.doDelete();
			}
		} );
		
		setResizable(false);
               
        }
        
        
        @Override
        public void actionPerformed(ActionEvent e) {
            String item = (String) jc1.getSelectedItem();
            Object o = subItems.get(item);
            if (o == null) {
                jc2.setModel(new DefaultComboBoxModel());
                //subComboBox2.setModel(new DefaultComboBoxModel());
            } else {
                jc2.setModel(new DefaultComboBoxModel((String[]) o));
                //subComboBox2.setModel(new DefaultComboBoxModel((String[]) o));
            }
        }
        
        @Override
        public void itemStateChanged(ItemEvent e) {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                if (e.getSource() == jc1) {
                    if (jc1.getSelectedIndex() != 0) {
                        FirstDialog firstDialog = new FirstDialog(cbpsGui.this,
                                jc1.getSelectedItem().toString(), "Please wait,  Searching for ..... ");
                    }
                } 
            }
        }
        
        private class FirstDialog extends JDialog {
            
            private static final long serialVersionUID = 1L;
            
            FirstDialog(final Frame parent, String winTitle, String msgString) {
                super(parent, winTitle);
                setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
                JLabel myLabel = new JLabel(msgString);
                JButton bNext = new JButton("Stop Processes");
                add(myLabel, BorderLayout.CENTER);
                add(bNext, BorderLayout.SOUTH);
                bNext.addActionListener(new ActionListener() {
                    
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        setVisible(false);
                    }
                });
                javax.swing.Timer t = new javax.swing.Timer(1000, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        setVisible(false);
                    }
                });
                t.setRepeats(false);
                t.start();
                setLocationRelativeTo(parent);
                setSize(new Dimension(400, 300));
                setVisible(true);
            }
        }
        
        
        public void showGui() {
            pack();
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            int centerX = (int)screenSize.getWidth() / 2;
            int centerY = (int)screenSize.getHeight() / 2;
            setLocation(centerX - getWidth() / 2, centerY - getHeight() / 2);
            super.setVisible(true);
        }
        
        
               
}