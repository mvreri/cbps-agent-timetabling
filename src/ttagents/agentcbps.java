package ttagents;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;


/**
 *
 * @author MURERI
 */
public class agentcbps extends Agent {
    
    private AID[] schAgents;
    private String cbpsReq, cbpsReq2, cbpsReq4, cbpsReq5;
    private int cbpsReq3;
    //private ttgui.cbpsGui myGui;
    private String sign="begin";
    
     public void setup(){
         
         System.out.println("Agent " + getAID().getName() + " is starting");
         //myGui = new ttgui.cbpsGui(this);
         //myGui.showGui();
         //myGui.fillCombo();
         //myGui.fillComboB();
         //myGui.fillCourse();
         
         
         addBehaviour(new TickerBehaviour(this, 2000){
             protected void onTick(){
                 DFAgentDescription dfd = new DFAgentDescription();
                 ServiceDescription cbpssd = new ServiceDescription();
                 cbpssd.setType("Start");
                 //cbpssd.setName("cbps");
                 dfd.addServices(cbpssd);
                 try {                      
                     DFAgentDescription[] result = DFService.search(myAgent, dfd);
                     System.out.println("Found the following agents");
                     schAgents = new AID[result.length];
                     for (int i = 0; i < result.length; ++i){
                         schAgents[i] = result[i].getName();
                         System.out.println(schAgents[i].getName());
                     }
                     if (result.length == 0){
                         myAgent.doDelete();
                     }
                     System.out.println();
                 } catch (FIPAException ex) {
                     ex.getACLMessage();
                     ex.printStackTrace();
                 }//end try-catch
                 
                 addBehaviour(new agentcbps.RequestPerformer());
             }//end onTick
             
         });//end AddBehaviour         
         
     }//end Setup
     
     
     protected void takeDown(){
         //myGui.dispose();
         System.out.println("Agent " + getAID().getName() + " is now terminating");
     }
     
     
     private class RequestPerformer extends Behaviour{
         private AID[] theSchools = schAgents;
         private int step = 0;
         private MessageTemplate mt;

        @Override
        public void action() {
            switch(step){
                case 0:
                    //send request to all the agents available
                    ACLMessage cfp = new ACLMessage(ACLMessage.CFP);
                    for (int i = 0; i < theSchools.length; i++){
                        cfp.addReceiver(theSchools[i]);
                        System.out.println("Message sent to " + theSchools[i].getName());//test
                    }
                    cfp.setContent(sign);
                    cfp.setConversationId("request");
                    cfp.setReplyWith("The CFP " + System.currentTimeMillis());
                    myAgent.send(cfp);
                    mt = MessageTemplate.and(MessageTemplate.MatchConversationId("request"),
                            MessageTemplate.MatchInReplyTo(cfp.getReplyWith()));
                    step = 1;
                    break;
                    
                case 1:
                    //receive messages from all the live agents
                    ACLMessage reply = myAgent.receive(mt);
                    if (reply != null){
                        if (reply.getPerformative() == ACLMessage.PROPOSE){
                            for (int i = 0; i < theSchools.length; i++){                                
                                String re = reply.getContent();
                                System.out.println("The response from schools is " + re + " from " + theSchools[i].getName());
                            }
                        }
                        step = 2;
                    }
                    else{
                        block();
                    }
                    break;
                    
                case 2:
                    //send message to all the different agents found
                    ACLMessage order = new ACLMessage(ACLMessage.ACCEPT_PROPOSAL);
                    for (int i = 0; i < theSchools.length; i++){
                        order.addReceiver(theSchools[i]);
                        System.out.println("Request sent to " + theSchools[i].getName());//test
                    }
                    order.setContent(sign);
                    order.setConversationId("request");
                    order.setReplyWith("The Order " + System.currentTimeMillis());
                    myAgent.send(order);
                    mt = MessageTemplate.and(MessageTemplate.MatchConversationId("request"),
                            MessageTemplate.MatchInReplyTo(order.getReplyWith()));
                    System.out.println("====================================================================");
                    step = 3;
                    myAgent.doDelete();
                    break;
                    
                case 3:
                    //receive message from the agents
                    reply = myAgent.receive(mt);
                    if (reply != null) {
                        // Purchase order reply received
                        if (reply.getPerformative() == ACLMessage.INFORM) {
                            // Purchase successful. We can terminate
                            for (int i=0; i<theSchools.length; i++){
                                theSchools[i] = reply.getSender(); 
                                System.out.println("Successfully acquired "+ reply.getContent()+" from agents "+ theSchools[i].getLocalName());                               
                            }
                            myAgent.doDelete();
                        }
                        else {
                            System.out.println("Attempt failed: requested sem already taken by "+ reply.getSender().getName());
                        }
                        
                        step = 4;
                    }
                    else {
                        block();
                    }
                    break;
                    /*
                case 4:
                    //send message to agents to begin negotiation with the L R and TS agents
                    order = new ACLMessage(ACLMessage.CONFIRM);
                    for(int i=0; i<theSchools.length; i++){
                        order.addReceiver(null);
                        
                    }*/
                    
            }//end switch
        }//end requestperformer.action

        @Override
        public boolean done() {
            //return step == 4;
            return false;
           
        }
         
     }//end requestperformer
     
     
     
     public void lookupData(final String SchoolName,final String courseName, final String Semester, final int classGroup){
    addBehaviour(new OneShotBehaviour(){
        public void action(){
          
                cbpsReq = SchoolName; //the school that should be queried
                cbpsReq2 = courseName; //the course for which the timetable is being sort
                cbpsReq4 = Semester; //value of the department
                cbpsReq3 = classGroup; //value of the semester requested
                //cbpsReq5 = classGroup; //the class for which the report is being sought
                        
                System.out.println(SchoolName +" entered. Passing values to school agents");                        
        }
    });
    }//end lookupData 
    
}//end agentcbps
