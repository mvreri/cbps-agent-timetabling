package ttagents;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;


/**
 *
 * @author MURERI
 */
public class agentGEOL extends Agent{
    
    private String reqGot, db1,db2,db3, db4,db5,db6,db9,trans="";
    private String[] sst;
    static ArrayList <String[]> ss;
    private int count,db7;
    private AID[] negAgents;
    private Connection conn = null;
    private Statement stmt=null,stmt2 = null;
    private ResultSet rs = null, rs2 = null;    
    private String dbURL = "jdbc:mysql://localhost/timetabling";
    private String username = "root";
    private String password = "";
    //private Vector datavect;
    private String ID = "5";//GEOL
    StringBuilder sb = new StringBuilder();
    
    @Override
    public void setup(){
        
        System.out.println("The agent " + getAID().getName() + " is now starting");
        
        //register with the agentcbps' df
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sdgeol = new ServiceDescription();
        sdgeol.setType("Start");
        sdgeol.setName(getLocalName());
        dfd.addServices(sdgeol);
        try {
            DFService.register(this, dfd);
        } catch (FIPAException ex) {
            ex.getMessage();
        }
        
        addBehaviour(new agentGEOL.getOrders());   
        
        //create a DF for the negotiating agents to register to
        addBehaviour(new TickerBehaviour(this, 12000){

            @Override
            protected void onTick() {
                DFAgentDescription dfd = new DFAgentDescription();
                ServiceDescription geolsd = new ServiceDescription();
                geolsd.setType("Nego");
                dfd.addServices(geolsd);
                try {                      
                    DFAgentDescription[] result = DFService.search(myAgent, dfd);
                    System.out.println("AgentGEOL: Found the following agents");
                    negAgents = new AID[result.length];
                    for (int i = 0; i < result.length; ++i){
                        negAgents[i] = result[i].getName();
                        System.out.println(negAgents[i].getName());
                    }
                    System.out.println();
                } catch (FIPAException ex) {
                    ex.getACLMessage();
                }//end try-catch
                
                addBehaviour(new agentGEOL.callAgents());
                
            }
            
        });
        
        addBehaviour(new agentGEOL.sendOrders());
    }//end setup
    
    @Override
    public void takeDown(){
        try {
            DFService.deregister(this);
        } catch (FIPAException ex) {
            ex.getMessage();
        }
        System.out.println("The agent " + getAID().getName() + " is now terminating");
        
    }//end takedown
    
    
    
    //get requests from agentCBPS 
    private class getOrders extends CyclicBehaviour{

        @Override
        public void action() {
            MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.CFP);
            ACLMessage req = myAgent.receive(mt);
            
            
            if (req != null){
                String reqGot = req.getContent();
                System.out.println("Get orders; received " + reqGot + " from " + req.getSender().getName());//test
                ACLMessage reply = req.createReply();
                System.out.println(reqGot);//test

                if(reqGot != null ){
                    
                    //get the value that has been requested by the cbps agent from db
                    try {
                        Class.forName("com.mysql.jdbc.Driver");
                    } catch (ClassNotFoundException ex) {
                        ex.getMessage();
                    }
                    try {
                        conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                        stmt = (Statement) conn.createStatement();
                        //get the ID of the requested School as well as the courses being offered by the school
                        ArrayList <String[]> result = new ArrayList<>();
                        stmt.execute("SELECT courses.CName, school.ID, school.SID, school.SName, department.SID, department.DID," 
                               +" department.DName, courses.ID, courses.CID   FROM school,courses,"
                               +"department WHERE department.ID = '"+ID+"' AND school.SID = department.SID AND  courses.DID =department.ID;"); 
                        rs = stmt.getResultSet();
                        
                        int columnCount = rs.getMetaData().getColumnCount();      
                        while (rs.next()) {     
                            String entry9 = rs.getString(1);//courses.cname 
                            String entry1 = rs.getString(2);//ID
                            String entry2 = rs.getString(3);//SID
                            String entry3 = rs.getString(4);//SName  
                            String entry4 = rs.getString(5);//department.sid  
                            String entry5 = rs.getString(6);//department.did  
                            String entry6 = rs.getString(7);//department.dname 
                            String entry7 = rs.getString(8);//courses.id 
                            String entry8= rs.getString(9);//courses.cid                 
                            
                            
                            
                            db1=entry1;//ID
                            db2=entry2;//SID
                            db3=entry3;//SName
                            db4=entry5;//did
                            db9=entry7;
                            
                            
                            //adding the values for the courseids to arraylist then to string for transmission to other agents
                            //from Stringbuilder
                            ArrayList<String> list = new ArrayList<>();
                            list.add(db9);                  
                            
                            System.out.println(db9);
                            
                                for (String s : list)
                                {
                                    sb.append(s);
                                    sb.append(" ");
                                }
                                
                           
                            //getting the numerical IDs of the courses in this department
                                //*****not using for now
                            String[] row = new String[columnCount];
                            for (int i=7; i <8 ; i++){//getting only the courses row
                                row[i] = rs.getString(i + 1);                               
                                
                                
                                
                                //System.out.println(row[i]);
                                
                                //sst[i]=row[i];
                                //System.out.println("The string is "+row[i]);
                            }
                            //trans = Arrays.toString(row);
                            result.add(row);    
                            
                        } 
                        trans = sb.toString();
                        //System.out.println("The final string isssss " + trans);
                        //System.out.println("The string is "+trans);
                        if (rs != null) {
                            try {
                                rs.close();
                            } catch (SQLException ex) { /* ignore */ }
                            rs = null;                    
                        }
                        if (stmt != null) {
                            try {
                                stmt.close();
                            } catch (SQLException ex) { /* ignore */ }
                            stmt = null;
                        }
                        if (conn != null) {
                            try {
                                conn.close();
                            } catch (SQLException ex) { /* ignore */ }
                            conn = null;
                        }          
                        
                    } catch (SQLException ex) {
                        ex.getMessage();
                    }//end database access
                    
                    
                    
                    
                    reply.setPerformative(ACLMessage.PROPOSE);
                    reply.setContent(trans);
                    
                    System.out.println(trans + " sent to " + req.getSender().getName() + " from" + getAID().getName());
                }
                else{
                    reply.setPerformative(ACLMessage.REFUSE);
                    reply.setContent("parameter-unavailable");
                    System.out.println("This" + getAID().getLocalName() + "isnt the agent you are looking for");
                    myAgent.doDelete();
                }                        
                myAgent.send(reply);
                
            }
            else{
                block();
            }
        }//end getOrders.action        

    }//end getOrders
    
    
    //respond to the requests from agentcbps
    private class sendOrders extends CyclicBehaviour{
        @Override
        public void action() {
            MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.ACCEPT_PROPOSAL);         
            ACLMessage req = myAgent.receive(mt);        
            
            if (req != null){
                reqGot = req.getContent();                
                ACLMessage reply = req.createReply();
                
                if (reply != null){
                    System.out.println("Send Orders; Successfully gotten "+ reqGot + " from " + req.getSender().getName());
                    
                    
                    //get the value that has been requested by the cbps agent from db
                    try {
                        Class.forName("com.mysql.jdbc.Driver");
                    } catch (ClassNotFoundException ex) {
                        ex.getMessage();
                    }
                    try {
                        conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                        stmt = (Statement) conn.createStatement();                        
                        
                        //get the courses being given by the different departments. this will facilitate the fetching of the course units
                        if (stmt.execute("SELECT ID,CID,CName FROM courses"
                                + " WHERE courses.CName ='"+reqGot+"';")) {
                            rs = stmt.getResultSet();
                        } else {                  
                            System.err.println("Select Failed");
                        }
                        
                        
                        Vector rows = new Vector();
                        Vector nrow;
                        int cnt = 0;
                        
                        while (rs.next()) {                        
                            String entry1 = rs.getString(2);//course names                            
                            String entry2 = rs.getString(3);//course IDs 
                            int entry3 = rs.getInt(1);//get the number of rows
                            
                            db7 = entry3;//id of the course selected by the user
                        }        
                        
                        
                        
                        
                        if (rs != null) {
                            try {
                                rs.close();
                            } catch (SQLException ex) { /* ignore */ }
                            rs = null;                    
                        }
                        if (stmt != null) {
                            try {
                                stmt.close();
                            } catch (SQLException ex) { /* ignore */ }
                            stmt = null;
                        }
                        if (conn != null) {
                            try {
                                conn.close();
                            } catch (SQLException ex) { /* ignore */ }
                            conn = null;
                        }          
                        
                    } catch (SQLException ex) {
                        ex.getMessage();                        
                    }//end database access
                    
                    
                   
                    
                    
                    reply.setPerformative(ACLMessage.INFORM);
                    reply.setContent(trans);
                    System.out.println(trans + " sent to " + req.getSender().getName() + " from" + getAID().getName());
                }
                else{
                    reply.setPerformative(ACLMessage.FAILURE);
                    reply.setContent("no-deal");
                }
                myAgent.send(reply);
            }
            else{
                block();
            }
        }//end sendOrders.action    
    }//end sendOrders  
    
    
    private class callAgents extends Behaviour{
        
        private MessageTemplate mt;
        private int step = 0;

        @Override
        public void action() {            
            switch(step){
                case 0:
                    //send message to ts, r and l
                    ACLMessage cfp = new ACLMessage(ACLMessage.CFP);
                    for (int i = 0; i < negAgents.length; i++){
                        cfp.addReceiver(negAgents[i]);
                        //System.out.println("agentGEOL: Communicaton with TS, L and R commenced");
                        
                    }
                    //agl.setArguments(arr);
                    cfp.setContent(ID);//what is to be passed to all the agents -deptname
                    cfp.setConversationId("negg");
                    cfp.setReplyWith("AgentGEOL CFP " + System.currentTimeMillis());
                    myAgent.send(cfp);
                    mt = MessageTemplate.and(MessageTemplate.MatchConversationId("negg"), MessageTemplate.MatchInReplyTo(cfp.getReplyWith()));
                    step = 1;
                    break;
                    
                case 1:
                    //get the response messages from the different agents
                    ACLMessage reply = myAgent.receive(mt);
                    if (reply != null){
                        if (reply.getPerformative() == ACLMessage.PROPOSE){
                            for (int i=0; i < negAgents.length; i++){
                                String reNeg = reply.getContent();
                                System.out.println("agentGEOL: Received " + reNeg + " from " + negAgents[i].getName());
                            }
                         
                        }
                        step = 2;
                    }
                    else{
                        block();
                    }
                      
                    break; 
                    
                case 2:
                    //send message to all the different agents found
                    ACLMessage order = new ACLMessage(ACLMessage.ACCEPT_PROPOSAL);
                    for (int i = 0; i < negAgents.length; i++){
                        order.addReceiver(negAgents[i]);
                        System.out.println("agentGEOL: Request sent to " + negAgents[i].getName());//test
                    }
                    order.setContent(trans);//the courses that need scheduling....for the department
                    order.setConversationId("negg");
                    order.setReplyWith("agentGEOL: The Order " + System.currentTimeMillis());
                    myAgent.send(order);
                    mt = MessageTemplate.and(MessageTemplate.MatchConversationId("negg"),
                            MessageTemplate.MatchInReplyTo(order.getReplyWith()));
                    System.out.println("====================================================================");
                    step = 3;
                    break;
                    
                case 3:
                    reply = myAgent.receive(mt);
                    if (reply != null) {
                        // Purchase order reply received
                        if (reply.getPerformative() == ACLMessage.INFORM) {
                            // Purchase successful. We can terminate
                            for (int i=0; i<negAgents.length; i++){
                                negAgents[i] = reply.getSender(); 
                                System.out.println("agentGEOL: Successfully acquired "+ reply.getContent()+" from agents "+ negAgents[i].getName());                               
                            }
                            myAgent.doDelete();
                        }
                        else {
                            System.out.println("agentGEOL: Attempt failed: requested sem already taken by "+ reply.getSender().getName());
                        }
                        
                        step = 4;
                    }
                    else {
                        block();
                    }
                    break;                    
            }//end switch                  
        }//end callAgents.action

        @Override
        public boolean done() {
            return step == 4;
        }
    }
    
}//end agentgeol
