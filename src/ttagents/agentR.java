package ttagents;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


/**
 *
 * @author MURERI
 */
public class agentR extends Agent {
    
    private AID[] otherAgents;
    private String id,rid,tran,trans;
    private Connection conn = null;
    private Statement stmt = null;
    private ResultSet rs = null;    
    private String dbURL = "jdbc:mysql://localhost/timetabling";
    private String username = "root";
    private String password = "";
    private String schID;//pass from the school agent
    StringBuilder sb = new StringBuilder();
    StringBuilder sbb = new StringBuilder();
    
    public void setup(){
        System.out.println("The agent " + getAID().getName() + " is now starting");
        
        //registering for the schools' DF
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sdR = new ServiceDescription();
        sdR.setType("Negor");
        //sdR.setType("NegAg");//registering for the TS-L-R's DF
        sdR.setName(getLocalName());
        dfd.addServices(sdR);
        
        /*
        //registering for the TS-L-R's DF
        DFAgentDescription Rdfd = new DFAgentDescription();
        Rdfd.setName(getAID());
        ServiceDescription Rsd = new ServiceDescription();
        Rsd.setType("NegAg");
        Rsd.setName("RAgent");
        Rdfd.addServices(Rsd);        
        */
        try {        
            DFService.register(this, dfd);
           // DFService.register(this, Rdfd);
        } catch (FIPAException ex) {
            ex.getMessage();
            ex.printStackTrace();
        }
        
        //functions
        addBehaviour(new agentR.getOrders());
        addBehaviour(new agentR.sendOrders());
        
    }
    
    public void takeDown(){
        try {
            DFService.deregister(this);
        } catch (FIPAException ex) {
            ex.getMessage();
            ex.printStackTrace();
        }
        
        System.out.println("Agent " + getAID().getName() + " is now terminating");        
    }
    
    private class getOrders extends CyclicBehaviour{
        private String reqGot = null;

        @Override
        public void action() {
            MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.CFP);
            ACLMessage req = myAgent.receive(mt);
            
            if (req != null){
                reqGot = req.getContent();
                System.out.println("AgentR: Get orders; received " + reqGot + " from " + req.getSender().getLocalName());//test
                ACLMessage reply = req.createReply();
                //System.out.println(reqGot);//test
                
               
                try {
                        Class.forName("com.mysql.jdbc.Driver");
                    } catch (ClassNotFoundException ex) {
                        ex.getMessage();
                        ex.printStackTrace();
                    }
                    try {
                        conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                        stmt = (Statement) conn.createStatement();
                        /*get the timeslot duration and number for a given class group in a given school
                         * avail these to the negotiation to guide in allocation by lecturers
                         * Parse the timeslot IDs to reveal the different class groups' */
                        //room ids
                        if (stmt.execute("SELECT DISTINCT RID FROM rooms where DID = '"+reqGot+"';")) {
                            rs = stmt.getResultSet();
                        } else {                  
                            System.err.println("Select Failed");
                        }
                        
                        while (rs.next()) {                        
                            String entry1 = rs.getString(1);//get the timeslot id booked     
                           
                            rid = entry1;
                            //System.out.println(rid);
                            
                            ArrayList<String> list = new ArrayList<String>();
                            list.add(rid);
                            for (String s : list)
                                {
                                    sb.append(s);
                                    sb.append(",");
                                }                                           
                        }
                        trans = sb.toString();
                        

                        
                        
                        if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException ex) { /* ignore */ }
                    rs = null;    
                }
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException ex) { /* ignore */ }
                    stmt = null;
                }
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException ex) { /* ignore */ }
                    conn = null;
                }          
                         
                        
                    } catch (SQLException ex) {
                        ex.getMessage();
                        ex.printStackTrace();
                    }//end database access
                

                if(reqGot != null){
                    
                    reply.setPerformative(ACLMessage.PROPOSE);
                    reply.setContent(trans);
                }
                else{
                    reply.setPerformative(ACLMessage.REFUSE);
                    reply.setContent("parameter-unavailable");
                }                        
                myAgent.send(reply);
            }
            else{
                block();
            }
        }
        
    }
    
    
    private class sendOrders extends CyclicBehaviour{
        private String reqGot = null;

        @Override
        public void action() {
            MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.AGREE);         
            ACLMessage req = myAgent.receive(mt);        
            
            if (req != null){
                reqGot = req.getContent();                
                ACLMessage reply = req.createReply();
                
                 try {
                        Class.forName("com.mysql.jdbc.Driver");
                    } catch (ClassNotFoundException ex) {
                        ex.getMessage();
                        ex.printStackTrace();
                    }
                    try {
                        conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                        stmt = (Statement) conn.createStatement();
                        /*get the timeslot duration and number for a given class group in a given school
                         * avail these to the negotiation to guide in allocation by lecturers
                         * Parse the timeslot IDs to reveal the different class groups' */
                        //room ids
                        if (stmt.execute("SELECT DISTINCT ID FROM rooms where DID = '"+reqGot+"' ;")) {
                            rs = stmt.getResultSet();
                        } else {                  
                            System.err.println("Select Failed");
                        }
                        
                        while (rs.next()) {                        
                            String entry1 = rs.getString(1);//get the timeslot id booked     
                           
                            id = entry1;
                            //System.out.println(rid);
                            
                            ArrayList<String> list = new ArrayList<String>();
                            list.add(id);
                            for (String s : list)
                                {
                                    sbb.append(s);
                                    sbb.append(",");
                                }                                           
                        }
                        tran = sbb.toString();
                        

                        
                        
                        if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException ex) { /* ignore */ }
                    rs = null;    
                }
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException ex) { /* ignore */ }
                    stmt = null;
                }
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException ex) { /* ignore */ }
                    conn = null;
                }          
                         
                        
                    } catch (SQLException ex) {
                        ex.getMessage();
                        ex.printStackTrace();
                    }//end database access
                
                
                
                
                if (reply != null){
                    System.out.println("AgentR: Send Orders; Successfully gotten "+ reqGot + " from " + req.getSender().getLocalName());
                    reply.setPerformative(ACLMessage.INFORM);
                    reply.setContent(tran);
                    System.out.println("AgentR: "+ tran +" sent to " + req.getSender().getLocalName() + " from" + getAID().getLocalName());
                }
                else{
                    reply.setPerformative(ACLMessage.FAILURE);
                    reply.setContent("no-deal");
                }
                myAgent.send(reply);
                myAgent.blockingReceive();
                
            }
            else{
                block();
            }
        }
        
    }
    
    
    private class negPartA extends CyclicBehaviour{

        @Override
        public void action() {
            try {
                Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException ex) {
                ex.getMessage();
                ex.printStackTrace();
            }
            try {
                conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                stmt = (Statement) conn.createStatement();
                /*get the rooms available in a particular department/school and the room types
                 also get the room types and capacities
                 Department is passed as a variable by school agent*/
                if (stmt.execute("SELECT RID, SID, RCapacity, RType FROM rooms "
                        + "WHERE rooms.SID ='"+ schID +"';")) {
                    rs = stmt.getResultSet();
                } else {                  
                    System.err.println("Select Failed");
                }
                
                while (rs.next()) {                        
                    String entry1 = rs.getString("SID");//SID                           
                    String entry3 = rs.getString(3);//SName     
                    
                    
                    ///////assign results to var for negotiation
                }
                
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException ex) { /* ignore */ }
                    rs = null;    
                }
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException ex) { /* ignore */ }
                    stmt = null;
                }
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException ex) { /* ignore */ }
                    conn = null;
                }          
                
            } catch (SQLException ex) {
                ex.getMessage();
                ex.printStackTrace();
            }//end database access
        }


    }  
}