package ttagents;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.wrapper.*;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author MURERI
 */
public class agentL extends Agent{
    
    private AID[] ts, r;
    private String lectID, negStuff,negRooms,negRoomIDs,Cyear,
            trans,trans2,cu,lid=null,lidx,cuid,culevel,ptid, deptid,ayrid,schtype,rmid;
    private String[] ss,timeslots,rooms,roomID,lectids,cuids,cus,culs,ptids,cours,tsss,
            lctids,lcids,ltids,lculs,lyids,
            lctidx,lcidx,ltidx,lculx,lyidx;
    private int rscount=0;
    
    
    private Connection conn = null, conn2 = null, conn3 = null, connn = null;
    private Statement stmt = null, stmt2=null,  stmtc = null,
            stmtn = null, stmtr = null, stmtq = null, stmts = null, stmtt = null, stmtu = null;
    private ResultSet rs=null,rs2=null, rsc = null,
            rsn = null,  rsr = null, rsq = null, rss = null, rst = null, rsu = null;    
    private String dbURL = "jdbc:mysql://localhost/timetabling";//retrieving for negotiation
    
    private String username = "root";
    private String password = "";
    
    public ttagents.agentSBS agtsbs;
    
    static String dept;//get the value sent from the college agent
    StringBuilder sb = new StringBuilder();
    StringBuilder sbb = new StringBuilder();
    StringBuilder sb0 = new StringBuilder();
    StringBuilder sb1 = new StringBuilder();
    StringBuilder sb2 = new StringBuilder();
    StringBuilder sb3 = new StringBuilder();
    StringBuilder sb4 = new StringBuilder();
    StringBuilder sb5 = new StringBuilder();
    StringBuilder sb6 = new StringBuilder();
    StringBuilder sb7 = new StringBuilder();
    StringBuilder sb8 = new StringBuilder();
    StringBuilder sb9 = new StringBuilder();
    StringBuilder sb10 = new StringBuilder();
    StringBuilder sb11 = new StringBuilder();
    StringBuilder sbc = new StringBuilder();
    StringBuilder sbn = new StringBuilder();
    StringBuilder sbn1 = new StringBuilder();
    StringBuilder sbn2 = new StringBuilder();
    StringBuilder sbn3 = new StringBuilder();
    StringBuilder sbn4 = new StringBuilder();
    
    
    StringBuilder sbna = new StringBuilder();
    StringBuilder sbnb = new StringBuilder();
    StringBuilder sbnc = new StringBuilder();
    StringBuilder sbnd = new StringBuilder();
    StringBuilder sbne = new StringBuilder();
    StringBuilder sbnf = new StringBuilder();
    
   
    StringBuilder sbnx1 = new StringBuilder();
    StringBuilder sbnx2 = new StringBuilder();
    StringBuilder sbnx3 = new StringBuilder();
    StringBuilder sbnx4 = new StringBuilder();
    StringBuilder sbnx5 = new StringBuilder();
    StringBuilder sbny = new StringBuilder();
    
    
    
    StringBuilder sbny1 = new StringBuilder();
    StringBuilder sbny2 = new StringBuilder();
    StringBuilder sbny3 = new StringBuilder();   

        
    
    
    
    
    public void setup(){
        
        
        System.out.println("The agent " + getAID().getName() + " is now starting");
        
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sdL = new ServiceDescription();
        sdL.setType("Nego");
        sdL.setName(getLocalName());
        dfd.addServices(sdL);
        
        
       
        
        try {        
            DFService.register(this, dfd);
            
        } catch (FIPAException ex) {
            ex.getMessage();
           
        }
        addBehaviour(new agentL.getOrders());//get orders from schoolagent
        addBehaviour(new agentL.sendOrders());
        
         addBehaviour(new TickerBehaviour(this, 14000){
            @Override
            protected void onTick() {
               
                reset();
                 
                DFAgentDescription dfd = new DFAgentDescription();
                ServiceDescription somsd = new ServiceDescription();
                somsd.setType("Negot");
                dfd.addServices(somsd);
                try {                      
                    DFAgentDescription[] result = DFService.search(myAgent, dfd);
                    System.out.println("AgentL: Looking for AgentTS");
                    ts = new AID[result.length];
                    for (int i = 0; i < result.length; ++i){
                        ts[i] = result[i].getName();
                        System.out.println(ts[i].getLocalName());
                    }
                    System.out.println();
                } catch (FIPAException ex) {
                    ex.getACLMessage();
                   
                }//end try-catch
                
                addBehaviour(new agentL.negPartA());//start communication with agentTS
                
            }            
        });
         
         //setup service for agentR
         addBehaviour(new TickerBehaviour(this, 14000){
            @Override
            protected void onTick() {
                
                reset();
                DFAgentDescription dfd = new DFAgentDescription();
                ServiceDescription somsd = new ServiceDescription();
                somsd.setType("Negor");
                dfd.addServices(somsd);
                try {                      
                    DFAgentDescription[] result = DFService.search(myAgent, dfd);
                    System.out.println("AgentL: Looking for AgentR");
                    r = new AID[result.length];
                    for (int i = 0; i < result.length; ++i){
                        r[i] = result[i].getName();
                        System.out.println(r[i].getLocalName());
                    }
                    System.out.println();
                } catch (FIPAException ex) {
                    ex.getACLMessage();
                    
                }//end try-catch
                
                addBehaviour(new agentL.negPartB());//start communication with agentTS
                
                
            }            
        });    
         
         

    }
    
    public void takeDown(){
        try {
            DFService.deregister(this);
        } catch (FIPAException ex) {
           ex.getMessage();
           
        }
        System.out.println("Agent " + getAID().getLocalName() + " is now terminating");
    }
    
    
    private static class getDept {

        public getDept(String reqGot) {
            dept = reqGot;
        }

    }
    
    //get initial message from the school agents
    public class getOrders extends CyclicBehaviour{
       
        @Override
        public void action() {
           
            String reqGot;
            
            MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.CFP);
            ACLMessage req = myAgent.receive(mt);
            
            if (req != null){
                reqGot = req.getContent();
                System.out.println("agentL: Get orders; received " + reqGot + " from " + req.getSender().getLocalName
());//test
                ACLMessage reply = req.createReply();
                
                if(reqGot != null){
                    getDept n = new getDept(reqGot);
                   
                    // get the unique lecturer ids from the preferences table///////////////////////////
                   
                    try {
                        Class.forName("com.mysql.jdbc.Driver");
                    } catch (ClassNotFoundException ex) {
                        ex.getMessage();
                    }
                    try {
                        lidx = null;
                        conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                        stmt = (Statement) conn.createStatement();
                        stmt.execute("SELECT DISTINCT lecturers.lid "
                                + "FROM lecturers, lecturerpref "
                                + "WHERE lecturers.id = lecturerpref.lectid "
                                + "AND lecturers.did =  '"+reqGot+"';");
                        rs = stmt.getResultSet();
                        
                        while (rs.next()) {                        
                            lidx = rs.getString(1);
                            
                             ArrayList<String> list = new ArrayList<String>();
                            list.add(lidx);                  
                                                        
                                for (String s : list)
                                {
                                    sb.append(s);
                                    sb.append(" ");
                                }
                                
                            
                        }
                        trans = sb.toString();//convert all the lecturers to string for transportation over acl   
                        
                        System.out.println("The string sent to school is "+trans);
                    } catch (SQLException ex) {
                        ex.getMessage();
                    }//end database access   
                    
                    reply.setPerformative(ACLMessage.PROPOSE);
                    reply.setContent(trans);
                    
                }
                else{
                    reply.setPerformative(ACLMessage.REFUSE);
                    reply.setContent("parameter-unavailable");
                }                        
                myAgent.send(reply);
            }
            else{
                block();
            }
            
            
        }//end getOrders.action    
             
    }//end getOrders
    
    
        
    
    public class sendOrders extends CyclicBehaviour{
        @Override
        public void action() {
            
            String reqGot;
            MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.ACCEPT_PROPOSAL);         
            ACLMessage req = myAgent.receive(mt);        
            
            if (req != null){
                reqGot = req.getContent();                
                ACLMessage reply = req.createReply();
                
                
               
                if (reply != null){
                    System.out.println("agentL: Send Orders; Successfully gotten "+ reqGot + " from " + req.getSender().getLocalName());
                    
                    cours = reqGot.split("\\s");//the courses in the department
                    for (int v = 0;v<cours.length;v++)
                        System.out.println(cours[v]);
                    //get the value that has been requested by the cbps agent
                    try {
                        Class.forName("com.mysql.jdbc.Driver");
                    } catch (ClassNotFoundException ex) {
                        ex.getMessage();                        
                    }
                    
                    try {
                        conn3 =(Connection) DriverManager.getConnection(dbURL, username, password);
                        stmtc = (Statement) conn3.createStatement();
                        
                        /*get the ID of the requested lecturer, and the preferences, and the unit he took
                         * Should return the name of the lecturer(for display), his ID, units teaching 
                         * as well as the slot preferences*/
                 
                        
                       
                        //get the schtype for the courses
                        
                        stmtc.execute("SELECT distinct timeslots.tid from courses, timeslots, schedule,department  "
                                + "WHERE department.ID ='"+dept+"' AND courses.did = department.id "
                                + "AND department.SchType = Schedule.SchType "
                                + "AND timeslots.SchType = Schedule.SchType;");
                        rsc = stmtc.getResultSet();
                        
                        
                        while (rsc.next()) {                        
                            String entry1 = rsc.getString(1);//get lecturer's LID
                            String tss = entry1;
                           
                            ArrayList<String> listtc = new ArrayList<String>();
                            listtc.add(tss);
                            for (String sc : listtc)
                            {
                                sbc.append(sc);
                                sbc.append(" ");
                            }
                                                    
                        }
                        String trc = sbc.toString();
                        tsss = trc.split("\\s");
                        for (int s=0;s<tsss.length;s++){
                            System.out.println("Matttteeeesssss zote" + tsss[s]);
                        }
                      
                        
                        if (rs != null) {
                            try {
                                rs.close();
                            } catch (SQLException ex) { /* ignore */ }
                            rs = null;    
                        }
                        if (stmt != null) {      
                            try {
                                stmt.close();
                            } catch (SQLException ex) { /* ignore */ }
                            stmt = null;
                        }
                        if (conn != null) {
                            try {
                                conn.close();
                            } catch (SQLException ex) { /* ignore */ }
                            conn = null;
                        }          
                         
                        
                    } catch (SQLException ex) {
                        ex.getMessage();
                        
                    }//end database access
                    
                    reply.setPerformative(ACLMessage.INFORM);
                    reply.setContent(lectID);
                    System.out.println(lectID + " sent to " + req.getSender().getName() + " from" + getAID().getLocalName());
                    
                }
                else{
                    reply.setPerformative(ACLMessage.FAILURE);
                    reply.setContent("no-deal");
                }
                myAgent.send(reply);
                //myAgent.doDelete();
                
            }
            else{
                block();
            }
        }//end sendOrders.action    
    }//end sendOrders  
    
    
    
    private class negPartA extends Behaviour{
        
        private MessageTemplate mt;
        private int step = 0;

        @Override
        public void action() {            
            switch(step){
                case 0:
                    
                    //send message to ts, r and l
                    ACLMessage cfp = new ACLMessage(ACLMessage.CFP);
                    for (int i = 0; i < ts.length; i++){
                        cfp.addReceiver(ts[i]);
                        //System.out.println("agentL: Communicaton with TS commenced");
                    }
                    
                    cfp.setContent(trans);//what is to be passed toagentts
                    cfp.setConversationId("negg");
                    cfp.setReplyWith("AgentL CFP " + System.currentTimeMillis());
                    myAgent.send(cfp);
                    mt = MessageTemplate.and(MessageTemplate.MatchConversationId("negg"), MessageTemplate.MatchInReplyTo(cfp.getReplyWith()));
                    step = 1;
                    break;
                    
                case 1:
                    //get the response messages from the different agents
                    ACLMessage reply = myAgent.receive(mt);
                    if (reply != null){
                        if (reply.getPerformative() == ACLMessage.PROPOSE){
                            for (int i=0; i < ts.length; i++){
                                String reNeg = reply.getContent();
                                negStuff = reNeg;//to facilitate negotiation later
                                System.out.println("agentL: Received " + reNeg + " from " + ts[i].getLocalName());          
                                             
                            }
                            //put the retrieved timeslots in an array
                            //System.out.println("And the timeslots are ");
                            timeslots = negStuff.split("\\s");
                        }
                        step = 2;
                    }
                    else{
                        block();
                    }
                    break; 
                    
                case 2:
                    //send message to ts to get the courseunit's year
                    ACLMessage order = new ACLMessage(ACLMessage.AGREE);
                    for (int i = 0; i < ts.length; i++){
                        order.addReceiver(ts[i]);
                        System.out.println("agentL: Request sent to " + ts[i].getLocalName());//test
                    }
                    order.setContent(trans);                     
                    order.setConversationId("negg");
                    order.setReplyWith("agentL: The Order " + System.currentTimeMillis());
                    myAgent.send(order);
                    mt = MessageTemplate.and(MessageTemplate.MatchConversationId("negg"),
                            MessageTemplate.MatchInReplyTo(order.getReplyWith()));
                    System.out.println("->->->->->->->TS.GetOrders->->->->->->->->->->");
                    step = 3;
                    break;
                    
                case 3:
                    reply = myAgent.receive(mt);
                    if (reply != null) {
                        // Purchase order reply received
                        if (reply.getPerformative() == ACLMessage.PROPOSE) {
                            // Purchase successful. We can terminate
                            
                            for (int i=0; i<ts.length; i++){
                                ts[i] = reply.getSender(); 
                                String repl = reply.getContent();
                                //the year of study of the given lecturer's preference
                                System.out.println("agentL: Acquired "+ repl+" from agents "+ ts[i].getLocalName());        
                       
                            }
                        }
                        step = 4;
                    }
                    else {
                        block();
                    }
                    break;
                    
                case 4:
                    //send message to ts to get the courseunit's year
                    ACLMessage order2 = new ACLMessage(ACLMessage.ACCEPT_PROPOSAL);
                    for (int i = 0; i < ts.length; i++){
                        order2.addReceiver(ts[i]);
                        System.out.println("agentL: 4_Request sent to " + ts[i].getName());//test
                    }
                    order2.setContent(trans);                     
                    order2.setConversationId("negg");
                    order2.setReplyWith("agentL: The Order " + System.currentTimeMillis());
                    myAgent.send(order2);
                    mt = MessageTemplate.and(MessageTemplate.MatchConversationId("negg"),
                            MessageTemplate.MatchInReplyTo(order2.getReplyWith()));
                    System.out.println("->->->->->->->TS->->->->->->->->->->");
                    step = 5;
                    break;
                    
                case 5:
                     reply = myAgent.receive(mt);
                    if (reply != null) {
                        // Purchase order reply received
                        if (reply.getPerformative() == ACLMessage.CONFIRM) {
                            // Purchase successful. We can terminate
                            
                            for (int i=0; i<ts.length; i++){
                                String repl = reply.getContent();
                                ts[i] = reply.getSender(); 
                                //the year of study of the given lecturer's preference
                                System.out.println("agentL: Successfully acquired "+ repl+" from agents "+ ts[i].getLocalName());                               
                            }
                        }
                        else {
                            System.out.println("agentL: Attempt failed: requested sem already taken by "+ reply.getSender().getLocalName());
                        }
                        
                        step = 6;
                    }
                    else {
                        block();
                    }
                    break;
                    
               
            }//end switch                  
        }//end callAgents.action

        @Override
        public boolean done() {
            return step == 6;
        }
    }

    
    private class negPartB extends Behaviour{
        
        private MessageTemplate mt;
        private int step = 0;

        @Override
        public void action() {            
            switch(step){
                case 0:
                    //send message to ts, r and l
                    ACLMessage cfp = new ACLMessage(ACLMessage.CFP);
                    for (int i = 0; i < r.length; i++){
                        cfp.addReceiver(r[i]);
                        System.out.println("agentL: Communicaton with R commenced");
                    }
                    cfp.setContent(dept);//what is to be passed toagentts
                    cfp.setConversationId("neggt");
                    cfp.setReplyWith("AgentL CFP " + System.currentTimeMillis());
                    myAgent.send(cfp);
                    mt = MessageTemplate.and(MessageTemplate.MatchConversationId("neggt"), MessageTemplate.MatchInReplyTo(cfp.getReplyWith()));
                    step = 1;
                    break;
                    
                case 1:
                    //get the response messages from the different agents
                    ACLMessage reply = myAgent.receive(mt);
                    if (reply != null){
                        if (reply.getPerformative() == ACLMessage.PROPOSE){
                            for (int i=0; i < r.length; i++){
                                String reNeg = reply.getContent();
                                negRooms = reNeg;//to facilitate negotiation later
                                System.out.println("agentL: Received " + reNeg + " from " + r[i].getLocalName());
                                                       
                            }
                           
                            //put all the rooms gotten into an array
                            rooms = negRooms.split(",");
                            
   
                        }
                        step = 2;
                    }
                    else{
                        block();
                    }
                    break; 
                    
                case 2:
                    //send message to ts to get the courseunit's year
                    ACLMessage order = new ACLMessage(ACLMessage.AGREE);
                    for (int i = 0; i < r.length; i++){
                        order.addReceiver(r[i]);
                        System.out.println("agentL: Request sent to " + r[i].getLocalName());//test
                    }
                    order.setContent(dept);                    
                    order.setConversationId("neggt");
                    order.setReplyWith("agentL: The Order " + System.currentTimeMillis());
                    myAgent.send(order);
                    mt = MessageTemplate.and(MessageTemplate.MatchConversationId("neggt"),
                            MessageTemplate.MatchInReplyTo(order.getReplyWith()));
                    System.out.println(">->->->->->->->->->->Rooms>->->->->->->->->->");
                    step = 3;
                    break;
                    
                case 3:
                    reply = myAgent.receive(mt);
                    if (reply != null) {
                        // Purchase order reply received
                        if (reply.getPerformative() == ACLMessage.INFORM) {
                            // Purchase successful. We can terminate
                            String repl = reply.getContent();
                            negRoomIDs = repl;
                            for (int i=0; i<r.length; i++){
                                r[i] = reply.getSender(); 
                                //the year of study of the given lecturer's preference
                                System.out.println("agentL: Successfully acquired "+ repl+" from agents "+ r[i].getLocalName());                               
                            }
                            
                            //System.out.println("And the Room IDs are ");
                            roomID = negRoomIDs.split(",");
                            
                            try {
                                Class.forName("com.mysql.jdbc.Driver");
                            } catch (ClassNotFoundException ex) {
                                ex.getMessage();
                                
                            }                  
                            try {
                                conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                                stmt = (Statement) conn.createStatement();
                                if (stmt.execute("SELECT distinct lecturerpref.tid  "
                                        + "FROM lecturerPref, Lecturers, courseunits,timeslots,schedule,department,courses, ayear "
                                        + "WHERE LecturerPref.LectID = Lecturers.ID "
                                        + "AND department.ID =  '"+dept+"' "
                                        + "AND lecturerpref.tid = timeslots.tid "
                                        + "AND lecturers.did = department.id "
                                        + "AND lecturerpref.cuid = courseunits.id "
                                        + "AND department.id = courses.did "
                                        + "AND courses.id = courseunits.cid "
                                        + "AND department.schtype = schedule.schtype "
                                        + "AND schedule.schtype = timeslots.schtype"
                                        + ";")){
                                    rs = stmt.getResultSet();
                                } else {                  
                                    System.err.println("Select Failed");
                                }
                                //int columnCount = rs.getMetaData().getColumnCount();
                                while (rs.next()) {        
                                   /* String entry1 = rs.getString(1);//get lecturer's LID    
                                    String entry2 = rs.getString(2);//courseunit name
                                    String entry3 = rs.getString(3);//courseunitid
                                    String entry4 = rs.getString(4);//courselevel*/
                                    String entry5 = rs.getString(1);//tid
                                   /* int entry6 = rs.getInt(6);//tstart
                                    int entry7 = rs.getInt(7);//tstop
                                    int entry8 = rs.getInt(8);//maxlength
                                    int entry9 = rs.getInt(9);//minlength
                                    String entry10 = rs.getString(10);//did
                                    String entry11 = rs.getString(11);//ayid
                                    
                                    
                                    lid=entry1;
                                    
                                    cuid = entry2;
                                    cu=entry3;
                                    culevel = entry4;*/
                                    ptid = entry5;
                                    /*tstart = entry6;
                                    tstop = entry7;
                                    maxl = entry8;
                                    minl = entry9;
                                    deptid = entry10;
                                    ayrid = entry11;
                                    
                                    ArrayList<String> listt = new ArrayList<String>();
                                    listt.add(lid);
                                    for (String s1 : listt)
                                    {
                                        sb1.append(s1);
                                        sb1.append(" ");
                                    }
                                    
                                    
                                    
                                    ArrayList<String> listt2 = new ArrayList<String>();
                                    listt2.add(cuid);
                                    for (String s2 : listt2)
                                    {
                                        sb2.append(s2);
                                        sb2.append(" ");
                                    }
                                    
                                    ArrayList<String> listt3 = new ArrayList<String>();
                                    listt3.add(cu);
                                    for (String s3 : listt3)
                                    {
                                        sb3.append(s3);
                                        sb3.append(" ");
                                    }
                                    
                                    
                                    ArrayList<String> listt4 = new ArrayList<String>();
                                    listt4.add(culevel);
                                    for (String s4 : listt4)
                                    {
                                        sb4.append(s4);
                                        sb4.append(" ");
                                    }
                                    */
                                    ArrayList<String> listt5 = new ArrayList<>();
                                    listt5.add(ptid);
                                    for (String s5 : listt5)
                                    {
                                        sb5.append(s5);
                                        sb5.append(" ");
                                    }
                                    /*
                                    ArrayList<Integer> listt6 = new ArrayList<Integer>();
                                    listt6.add(tstart);
                                    for (int s6 : listt6)
                                    {
                                        sb6.append(s6);
                                        sb6.append(" ");
                                    }
                                    
                                    ArrayList<Integer> listt7 = new ArrayList<Integer>();
                                    listt7.add(tstop);
                                    for (int s7 : listt7)
                                    {
                                        sb7.append(s7);
                                        sb7.append(" ");
                                    }
                                    
                                    ArrayList<Integer> listt8 = new ArrayList<Integer>();
                                    listt8.add(maxl);
                                    for (int s8 : listt8)
                                    {
                                        sb8.append(s8);
                                        sb8.append(" ");
                                    }
                                    
                                    ArrayList<Integer> listt9 = new ArrayList<Integer>();
                                    listt9.add(minl);
                                    for (int s9 : listt9)
                                    {
                                        sb9.append(s9);
                                        sb9.append(" ");
                                    }
                                    
                                     ArrayList<String> listt10 = new ArrayList<String>();
                                    listt10.add(deptid);
                                    for (String s10 : listt10)
                                    {
                                        sb10.append(s10);
                                        sb10.append(" ");
                                    }
                                    
                                    
                                     ArrayList<String> listt11 = new ArrayList<String>();
                                    listt11.add(ayrid);
                                    for (String s11 : listt11)
                                    {
                                        sb11.append(s11);
                                        sb11.append(" ");
                                    }
                                    */
                                }
                                
                               try {
                                    rs.last();
                                    rscount = rs.getRow();
                                } catch (SQLException exp) {
                                    exp.getMessage();
                                } finally {
                                    try {
                                        rs.beforeFirst();
                                    } catch (SQLException exp) {
                                       exp.getMessage();
                                    }
                                }
                                
                                //System.out.println("The first count is jnkjnfkgdfkjbg: "+rscount);  
                                
                                } catch (SQLException ex) {
                                ex.getMessage();
                                
                            
                            }//end database access
                            
                            if (rscount == 0){//if the query returns no results
                                System.out.println("No preferences have been entered by lecturers in this department\n or"
                                        + " the courses have already been allocated");
                                
                                myAgent.blockingReceive();
                                

                                //addBehaviour(new ttagents.agentL.sendOrders() );
                                
                                
                            }
                            else if (rscount == 1){
                                try {
                                    Class.forName("com.mysql.jdbc.Driver");
                                } catch (ClassNotFoundException ex) {
                                    ex.getMessage();
                                    
                                }                  
                                try {
                                    conn2 =(Connection) DriverManager.getConnection(dbURL, username, password);
                                    stmt2 = (Statement) conn2.createStatement();
                                    //insert the value into the timetable
                                    stmt2.execute("INSERT INTO TT (TTNegCU, TTNegLID, TTNegRID, TTNegTID, TTNegAcademicYear)"
                                            + " VALUES ('"+cu+"','"+lid+"', '', '', '');");
                                    rs2 = stmt2.getResultSet();
                                    
                                    
                                    if (rs2 != null) {
                                        try {
                                            rs2.close();
                                        } catch (SQLException ex) { /* ignore */ }
                                        rs2 = null;    
                                    }
                                    if (stmt2 != null) {   
                                        try {
                                            stmt2.close();
                                        } catch (SQLException ex) { /* ignore */ }
                                        stmt2 = null;
                                    }
                                    if (conn2 != null) {
                                        try {
                                            conn2.close();
                                        } catch (SQLException ex) { /* ignore */ }
                                        conn2 = null;
                                    }                               
                                    
                                } catch (SQLException ex) {
                                    ex.getMessage();
                                    
                                }//end database access
                                
                            myAgent.blockingReceive();
                            
                            } 
                            else{//both have selected same slot for the same level in a given *degree program
                                /*String tr = sb1.toString();
                                lectids = tr.split("\\s");//timeslots
                                String tr2 = sb2.toString();
                                cuids = tr2.split("\\s");
                                String tr3 = sb3.toString();
                                cus = tr3.split("\\s");
                                String tr4 = sb4.toString();
                                culs = tr4.split("\\s");*/
                                String tr5 = sb5.toString();
                                ptids = tr5.split("\\s");
                               
                               /* 
                                String tr6 = sb6.toString();
                                tstarts = tr6.split("\\s");
                                String tr7 = sb7.toString();
                                tstops = tr7.split("\\s");                                 
                                String tr8 = sb8.toString();
                                maxls = tr8.split("\\s");
                                String tr9 = sb9.toString();
                                minls = tr9.split("\\s");
                                String tr10 = sb10.toString();
                                deptids = tr10.split("\\s");
                                String tr11 = sb11.toString();
                                ayrids = tr11.split("\\s");
                                
                                */
                                System.out.println("AgentL: Negotiating for slots " + negStuff + " beginning");
                                //myAgent.blockingReceive();
          
         
                                addBehaviour(new agentL.genTT());
                                
                                
                              
                            }
                        }
                        else {
                            System.out.println("agentL: Attempt failed: requested sem already taken by "+ reply.getSender().getLocalName());
                        }
                        
                        step = 4;
                    }
                    else {
                        block();
                    }
                    break;                    
            }//end switch                  
        }//end callAgents.action

        @Override
        public boolean done() {
            return step == 4;
        }
    }
    
  
    
    private class genTT extends Behaviour{
        
        int rno=0;
        String rooom=null,yrs=null,
                slid,stid,scuid,sayid, lectprefid, lectid, tid,lpcuid,ayid;
        String[] tslots, clevels, tslots1, clevels1, lpids,lassts,bs,cs,ds,es,fs,tses,ttlid,tttid,ttcu;
        int mark,mark2,m1cnt,lptid,lclvl,less,cntr,ttcnt,schcnt,tidcount, ttcnt2;
         int[] sltx,lpds,lsts,lculsr, lculsrx,lasstsr,lpidsr,tsesi;

        @Override
        public void action() {
            
            try {//get the number of times that the course has been scheduled so far and limit it to a max of 2
                        connn =(Connection) DriverManager.getConnection(dbURL, username, password);
                        stmtq = (Statement) connn.createStatement(); 
                        stmtq.execute("UPDATE lecturerpref SET assignstatus=0 WHERE assignstatus=1;");
                        rsq = stmtq.getResultSet();
                       
                        if (rsq != null) {
                        try {
                            rsq.close();
                        } catch (SQLException ex) {  }
                        rsq = null;    
                        }             
                        
                        if (stmtq != null) { 
                            try {
                                stmtq.close();
                            } catch (SQLException ex) { }
                            stmtq = null;
                        }
                        if (connn != null) {
                            try {
                                connn.close();
                            } catch (SQLException ex) {  }
                            connn = null;
                        }        
                    } catch (SQLException ex) {
                        ex.getMessage();
                    }//end database access
                
            
            for (int q=0;q<cours.length;q++){
            //for all the unique timeslots,
            for (int a=0;a<ptids.length;a++){               
                try {
                    connn =(Connection) DriverManager.getConnection(dbURL, username, password);
                    stmtn = (Statement) connn.createStatement(); 
                    stmtn.execute("select distinct lecturerpref.lectid, lecturerpref.cuid, lecturerpref.tid, courseunits.culevel, lecturerpref.ayid "
                            + "from lecturerpref, courseunits where courseunits.id = lecturerpref.cuid and tid = '"+ptids[a]+"'");
                    rsn = stmtn.getResultSet();
                    while (rsn.next()){
                        String lctid = rsn.getString(1);
                        String lcid = rsn.getString(2);
                        String ltid = rsn.getString(3);
                        String lcul = rsn.getString(4);
                        String lyid = rsn.getString(5);
                        
                        ArrayList<String> tss1 = new ArrayList<>();
                        tss1.add(lctid);
                        for (String ts1 : tss1)
                        {
                            sbn.append(ts1);
                            sbn.append(" ");
                        }
                        ArrayList<String> tss2 = new ArrayList<>();
                        tss2.add(lcid);
                        for (String ts2 : tss2)
                        {
                            sbn1.append(ts2);
                            sbn1.append(" ");
                        }                       
                        ArrayList<String> tss3 = new ArrayList<>();
                        tss3.add(ltid);
                        for (String ts3 : tss3)
                        {
                            sbn2.append(ts3);
                            sbn2.append(" ");
                        }
                        ArrayList<String> tss4 = new ArrayList<>();
                        tss4.add(lcul);
                        for (String ts4 : tss4)
                        {
                            sbn3.append(ts4);
                            sbn3.append(" ");
                        }
                        ArrayList<String> tss5 = new ArrayList<>();
                        tss5.add(lyid);
                        for (String ts5 : tss5)
                        {
                            sbn4.append(ts5);
                            sbn4.append(" ");
                        }
                        
                    }
                    //PLACE RESULTS OF LECTURER PREFERENCES IN ARRAYS
                    String ar = sbn.toString();
                    lctids = ar.split("\\s");//lectid
                    System.out.print(lctids[a]);
                    
                    
                    String ar2 = sbn1.toString();
                    lcids = ar2.split("\\s");//cuid
                    System.out.print(lcids[a]);
                    
                    String ar3 = sbn2.toString();
                    ltids = ar3.split("\\s");//tid
                    System.out.print(ltids[a]);
                    
                    String ar4 = sbn3.toString();
                    lculs = ar4.split("\\s");//courselevel
                    System.out.println(lculs[a]);
                    
                    String ar5 = sbn4.toString();
                    lyids = ar5.split("\\s");//academic year
                    System.out.println(lyids[a]);
                    
                    if (rsn != null) {
                        try {
                            rsn.close();
                        } catch (SQLException ex) {  }
                        rsn = null;    
                    }                        
                    if (stmtn != null) {   
                        try {
                            stmtn.close();
                        } catch (SQLException ex) { }
                        stmtn = null;
                    }
                    if (connn != null) {
                        try {
                            connn.close();
                        } catch (SQLException ex) {  }
                        connn = null;
                    }        
                } catch (SQLException ex) {
                    ex.getMessage();
                }//end database access
                
                
                
                
                //////START THE NEGOTIATIONS IF ANY
                
                
                lculsr=new int[lculs.length];
                for (int g=0;g<lculs.length;g++){
                    lculsr[g] = Integer.parseInt(lculs[g]);
                }
                
                
                
                
            
            
            
            for (int r=0;(r<ltids.length) && (r<lcids.length) && (r<lctids.length) && (r<lyids.length);r++){
                
                try {//get the number of times that the course has been scheduled so far and limit it to a max of 2
                        connn =(Connection) DriverManager.getConnection(dbURL, username, password);
                        stmtq = (Statement) connn.createStatement(); 
                        stmtq.execute("SELECT count(DISTINCT ttnegtid) FROM tt WHERE ttnegcu = '"+lcids[r]+"';");
                        rsq = stmtq.getResultSet();
                        while (rsq.next()){
                            ttcnt = rsq.getInt(1);
                        }
                        
                        System.out.println("The tttttttcount for "+ltids[r]+" is "+ ttcnt);
                        if (rsq != null) {
                        try {
                            rsq.close();
                        } catch (SQLException ex) {  }
                        rsq = null;    
                        }             
                        
                        if (stmtq != null) { 
                            try {
                                stmtq.close();
                            } catch (SQLException ex) { }
                            stmtq = null;
                        }
                        if (connn != null) {
                            try {
                                connn.close();
                            } catch (SQLException ex) {  }
                            connn = null;
                        }        
                    } catch (SQLException ex) {
                        ex.getMessage();
                    }//end database access
                    
                
                
                
                
                
                    try {
                        connn =(Connection) DriverManager.getConnection(dbURL, username, password);
                        stmtq = (Statement) connn.createStatement(); 
                        stmtq.execute("SELECT count(lecturerpref.tid) FROM lecturerpref, courseunits "
                                + "WHERE lecturerpref.tid = '"+ltids[r]+"' AND courseunits.id = lecturerpref.cuid AND courseunits.culevel = "+lculsr[r]+";");
                        rsq = stmtq.getResultSet();
                        while (rsq.next()){
                            m1cnt = rsq.getInt(1);
                        }
                        
                        System.out.println("The count is "+ m1cnt);
                        if (rsq != null) {
                        try {
                            rsq.close();
                        } catch (SQLException ex) {  }
                        rsq = null;    
                        }             
                        
                        if (stmtq != null) { 
                            try {
                                stmtq.close();
                            } catch (SQLException ex) { }
                            stmtq = null;
                        }
                        if (connn != null) {
                            try {
                                connn.close();
                            } catch (SQLException ex) {  }
                            connn = null;
                        }        
                    } catch (SQLException ex) {
                        ex.getMessage();
                    }//end database access
                    
                    
                    if (m1cnt==1){//this is the only entry then
                        
                        if(ttcnt<2){
                        
                        try{                              
                            conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                            stmtr = (Statement) conn.createStatement(); 
                            stmtr.execute("SELECT lecturerpref.lectprefid, lecturerpref.tid, lecturerpref.assignstatus, courseunits.culevel "
                                    + "FROM lecturerpref,courseunits "
                                    + "WHERE lecturerpref.tid = '"+ltids[r]+"' "
                                    + "AND courseunits.id = lecturerpref.cuid AND courseunits.culevel ="+lculsr[r]+";");
                            rsr = stmtr.getResultSet();            
                            
                            while (rsr.next()){
                                String lpid = rsr.getString(1);//lectprefid
                                String lasst = rsr.getString(3);//assignstatus
                                
                                ArrayList<String> tss1 = new ArrayList<>();
                                tss1.add(lpid);
                                for (String ts1 : tss1)
                                {
                                    sbna.append(ts1);
                                    sbna.append(" ");
                                }
                                ArrayList<String> tss2 = new ArrayList<>();
                                tss2.add(lasst);
                                for (String ts2 : tss2)
                                {
                                    sbnb.append(ts2);
                                    sbnb.append(" ");
                                }              
                            }
                            if (rsr != null) {
                                try {
                                    rsr.close();
                                } catch (SQLException ex) {  }
                                rsr = null;    
                            }                     
                            if (stmtr != null) {  
                                try {
                                    stmtr.close();
                                } catch (SQLException ex) { }
                                stmtr = null;
                            }
                            if (conn != null) {
                                try {
                                    conn.close();
                                } catch (SQLException ex) {  }
                                conn = null;
                            }        
                        } catch (SQLException ex) {
                            ex.getMessage();
                        }//end database access
                        
                        
                        String ar = sbna.toString();
                        lpids = ar.split("\\s");//lectid
                        
                        lpidsr=new int[lpids.length];
                        for (int g=0;g<lpids.length;g++){
                            lpidsr[g] = Integer.parseInt(lpids[g]);
                        }
                        
                        
                        try{   
                            //get the room to be used
                            conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                            stmtr = (Statement) conn.createStatement();
                            stmtr.execute("SELECT DISTINCT roomkb.rid FROM roomkb, courseunits  "
                                    + "WHERE roomkb.possibleunit = courseunits.cuid and courseunits.id = '"+lcids[r]+"' "
                                    + "AND courseunits.culevel =  "+lculsr[r]+";");
                            rsr = stmtr.getResultSet();
                            while (rsr.next()){
                                rooom = rsr.getString(1);    
                            }
                            //System.out.println("Allocated room "+rooom+" to "+lcids[r]+" on class "+ lculsr[r]);
                            if (rsr != null) {
                                try {
                                    rsr.close();
                                } catch (SQLException ex) {  }
                                rsr = null;    
                            }                        
                            if (stmtr != null) {   
                                try {
                                    stmtr.close();
                        } catch (SQLException ex) { }
                                stmtr = null;
                            }
                            if (connn != null) {
                                try {
                                    connn.close();
                                } catch (SQLException ex) {  }
                                connn = null;
                            }        
                        } catch (SQLException ex) {
                            ex.getMessage();
                        }//end database access
                    
                       
                        
                         try{   
                        //commit all the details to the tt table 
                        conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                        stmtt = (Statement) conn.createStatement(); 
                        stmtt.execute("INSERT INTO TT (TTNegCU, TTNegLID, TTNegRID, TTNegTID, TTNegAcademicYear) "
                                + "VALUES ('"+lcids[r]+"','"+lctids[r]+"', '"+rooom+"', '"+ltids[r]+"', '"+lyids[r]+"');");
                        rst = stmtt.getResultSet();  
                        if (rst != null) {
                        try {
                            rst.close();
                        } catch (SQLException ex) {  }
                        rst = null;    
                        }                 
                        
                        if (stmtt != null) {  
                            try {
                                stmtt.close();
                            } catch (SQLException ex) { }
                            stmtt = null;
                        }
                        if (conn != null) {
                            try {
                                conn.close();
                            } catch (SQLException ex) {  }
                            conn = null;
                        }        
                     } catch (SQLException ex) {
                         ex.getMessage();
                     }//end database access
                     
                     try{
                         conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                         stmtu = (Statement) conn.createStatement(); 
                         stmtu.execute("UPDATE lecturerpref SET AssignStatus='1' where lectprefid = '"+lpids[r]+"';");
                         rsu = stmtu.getResultSet();     
                         if (rsu != null) {
                             try {
                                 rsu.close();
                             } catch (SQLException ex) {  }
                             rsu = null;    
                         }               
                         
                         if (stmtu != null) {                               
                             try {
                                 stmtu.close();
                             } catch (SQLException ex) { }
                             stmtu = null;
                         }
                         if (connn != null) {
                             try {
                                 connn.close();
                             } catch (SQLException ex) {  }
                             connn = null;
                         }        
                     } catch (SQLException ex) {
                         ex.getMessage();
                     }//end database access
                     
                        System.out.println("Scheduled the  course");
                   
                    }
                    else{
                        System.out.println("The maximum schedule for the course has been reached");
                    }
                    
                    }                  
                    else{//IF THE COURSE IS NOT UNIQUE
                        
                        try{                              
                            conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                            stmtr = (Statement) conn.createStatement(); 
                            stmtr.execute("SELECT lecturerpref.lectprefid, lecturerpref.tid, lecturerpref.assignstatus, courseunits.culevel "
                                    + "FROM lecturerpref,courseunits "
                                    + "WHERE lecturerpref.tid = '"+ltids[r]+"' "
                                    + "AND courseunits.id = lecturerpref.cuid AND courseunits.culevel ="+lculsr[r]+";");
                            rsr = stmtr.getResultSet();            
                            
                            while (rsr.next()){
                                String lpid = rsr.getString(1);//lectprefid
                                String lasst = rsr.getString(3);//assignstatus
                                ArrayList<String> tss1 = new ArrayList<>();
                                tss1.add(lpid);
                                for (String ts1 : tss1)
                                {
                                    sbna.append(ts1);
                                    sbna.append(" ");
                                }
                                ArrayList<String> tss2 = new ArrayList<>();
                                tss2.add(lasst);
                                for (String ts2 : tss2)
                                {
                                    sbnb.append(ts2);
                                    sbnb.append(" ");
                                }              
                            }
                            if (rsr != null) {
                                try {
                                    rsr.close();
                                } catch (SQLException ex) {  }
                                rsr = null;    
                            }                     
                            if (stmtr != null) {  
                                try {
                                    stmtr.close();
                                } catch (SQLException ex) { }
                                stmtr = null;
                            }
                            if (conn != null) {
                                try {
                                    conn.close();
                                } catch (SQLException ex) {  }
                                conn = null;
                            }        
                        } catch (SQLException ex) {
                            ex.getMessage();
                        }//end database access
                        
                        
                        String ar = sbna.toString();
                        lpids = ar.split("\\s");//lectid
                        
                        lpidsr=new int[lpids.length];
                        for (int g=0;g<lpids.length;g++){
                            lpidsr[g] = Integer.parseInt(lpids[g]);
                        }
                        
                        //CONVERT ASSIGNSTATUS TO INTEGER
                        String ar2 = sbnb.toString();
                        lassts = ar2.split("\\s");//cuid
                        lasstsr=new int[lassts.length];
                        for (int g=0;g<lassts.length;g++){
                            lasstsr[g] = Integer.parseInt(lassts[g]);
                        }
                        
                        
                        //check number of occurrences of the cu so far
                        try{ 
                            conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                            stmtr = (Statement) conn.createStatement();
                            stmtr.execute("SELECT count(DISTINCT ttneglid, ttnegtid)"
                                    + " FROM tt, lecturerpref WHERE tt.ttneglid = lecturerpref.lectid "
                                    + "AND lecturerpref.lectid = '"+lctids[r]+"';");
                            rsr = stmtr.getResultSet();
                            while (rsr.next()){
                                cntr = rsr.getInt(1);    
                            }
                            
                            //System.out.println("The coooooooooooount "+cntr);
                            
                            if (rsr != null) {
                                try {
                                    rsr.close();
                                } catch (SQLException ex) {  }
                                rsr = null;    
                            }                     
                            if (stmtr != null) {   
                                try {
                                    stmtr.close();
                                } catch (SQLException ex) { }
                                stmtr = null;
                            }
                            if (connn != null) {
                                try {
                                    connn.close();
                                } catch (SQLException ex) {  }
                                connn = null;
                            }        
                        } catch (SQLException ex) {
                            ex.getMessage();
                        }//end database access
                    
                  
                        if(m1cnt==2){
                        for (int tt=0;tt<lpidsr.length && tt<lasstsr.length;tt++){
                            for (int tu=tt+1;tu<lpidsr.length && tu<lasstsr.length;tu++){
                                
                                    if ((lpidsr[tt]) < (lpidsr[tu]) && ((lpidsr[tt])%2== 0) ) {
                                        if(lasstsr[tt]>=lasstsr[tu] && tt!=tu){
                                            less = lpidsr[tt];
                                        }
                                        else{
                                            less = lpidsr[tu];
                                        }
                                    }else{
                                        less = lpidsr[tu];
                                    }
                                
                            }
                        }
                        }else if(m1cnt==3){
                            for (int tt=0;tt<lpidsr.length && tt<lasstsr.length;tt++){
                            for (int tu=tt+1;tu<lpidsr.length && tu<lasstsr.length;tu++){
                                
                                    if ((lpidsr[tt]) < (lpidsr[tu]) && ((lpidsr[tt])%4== 1) ) {
                                        if(lasstsr[tt]>=lasstsr[tu] && tt!=tu){
                                            less = lpidsr[tt];
                                        }
                                        else{
                                            less = lpidsr[tu];
                                        }
                                    }else{
                                        less = lpidsr[tu];
                                    }
                                
                            }
                            }   
                        }
                        else if(m1cnt == 4){
                            for (int tt=0;tt<lpidsr.length && tt<lasstsr.length;tt++){
                            for (int tu=tt+1;tu<lpidsr.length && tu<lasstsr.length;tu++){
                                
                                    if ((lpidsr[tt]) < (lpidsr[tu]) && ((lpidsr[tt])%5== 2) ) {
                                        if(lasstsr[tt]>=lasstsr[tu] && tt!=tu){
                                            less = lpidsr[tt];
                                        }
                                        else{
                                            less = lpidsr[tu];
                                        }
                                    }else{
                                        less = lpidsr[tu];
                                    }
                                
                            }
                            }   
                        }
                       
                        //System.out.println("Oh YEEfgfgdfdgEEEAh 2 m1s");
                        //System.out.println(less);
                        try{
                        conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                        stmts = (Statement) conn.createStatement(); 
                        stmts.execute("SELECT lectid,cuid,tid,ayid FROM lecturerpref WHERE lectprefid = '"+less+"';");
                        rss = stmts.getResultSet();
                         while (rss.next()){
                            slid = rss.getString(1);//lectprefid
                            scuid = rss.getString(2);//cuid
                            stid = rss.getString(3);//assignstatus
                            sayid = rss.getString(4);//assignstatus
                         
                         }
                         
                         if (rss != null) {
                             try {
                                 rss.close();
                             } catch (SQLException ex) {  }
                             rss = null;    
                         }                        
                         if (stmts != null) {   
                             try {
                                 stmts.close();
                             } catch (SQLException ex) { }
                             stmts = null;
                         }
                         if (conn != null) {
                             try {
                                 conn.close();
                             } catch (SQLException ex) {  }
                             conn = null;
                         }        
                        } catch (SQLException ex) {
                            ex.getMessage();
                        }//end database access
                        
                        
                        try{   
                            //get the room to be used
                            conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                            stmtr = (Statement) conn.createStatement();
                            stmtr.execute("SELECT DISTINCT roomkb.rid FROM roomkb, courseunits  "
                                    + "WHERE roomkb.possibleunit = courseunits.cuid and courseunits.id = '"+lcids[r]+"' "
                                    + "AND courseunits.culevel =  "+lculsr[r]+";");
                            rsr = stmtr.getResultSet();
                            while (rsr.next()){
                                rooom = rsr.getString(1);    
                            }
                            //System.out.println("Allocated room "+rooom+" to "+lcids[r]+" on class "+ lculsr[r]);
                            if (rsr != null) {
                                try {
                                    rsr.close();
                                } catch (SQLException ex) {  }
                                rsr = null;    
                            }                        
                            if (stmtr != null) {   
                                try {
                                    stmtr.close();
                        } catch (SQLException ex) { }
                                stmtr = null;
                            }
                            if (connn != null) {
                                try {
                                    connn.close();
                                } catch (SQLException ex) {  }
                                connn = null;
                            }        
                        } catch (SQLException ex) {
                            ex.getMessage();
                        }//end database access
                    
                        
                     try{   
                        //commit all the details to the tt table 
                        conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                        stmtt = (Statement) conn.createStatement(); 
                        stmtt.execute("INSERT INTO TT (TTNegCU, TTNegLID, TTNegRID, TTNegTID, TTNegAcademicYear) "
                                + "VALUES ('"+scuid+"','"+slid+"','"+rooom+"',  '"+stid+"', '"+sayid+"');");
                        rst = stmtt.getResultSet();  
                        if (rst != null) {
                        try {
                            rst.close();
                        } catch (SQLException ex) {  }
                        rst = null;    
                        }                 
                        
                        if (stmtt != null) {  
                            try {
                                stmtt.close();
                            } catch (SQLException ex) { }
                            stmtt = null;
                        }
                        if (conn != null) {
                            try {
                                conn.close();
                            } catch (SQLException ex) {  }
                            conn = null;
                        }        
                     } catch (SQLException ex) {
                         ex.getMessage();
                     }//end database access
                     
                     try{
                         conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                         stmtu = (Statement) conn.createStatement(); 
                         stmtu.execute("UPDATE lecturerpref SET AssignStatus='1' where lectprefid = '"+less+"';");
                         rsu = stmtu.getResultSet();     
                         if (rsu != null) {
                             try {
                                 rsu.close();
                             } catch (SQLException ex) {  }
                             rsu = null;    
                         }               
                         
                         if (stmtu != null) {                               
                             try {
                                 stmtu.close();
                             } catch (SQLException ex) { }
                             stmtu = null;
                         }
                         if (connn != null) {
                             try {
                                 connn.close();
                             } catch (SQLException ex) {  }
                             connn = null;
                         }        
                     } catch (SQLException ex) {
                         ex.getMessage();
                     }//end database access
                     
                     
                     
                     System.out.println("Finished Negotiation");    
                     
                    }//END ELSE
                    
                }//END SECOND FOR LOOP ----   
            
            
               }//end ptids[a] 
            
            
            for (int b=0;b<ptids.length;b++){
                
                try {
                    conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                    stmtn = (Statement) conn.createStatement(); 
                    stmtn.execute("select distinct lecturerpref.lectid, lecturerpref.cuid, lecturerpref.tid, courseunits.culevel, lecturerpref.ayid "
                            + "from lecturerpref, courseunits where courseunits.id = lecturerpref.cuid and tid = '"+ptids[b]+"' and assignstatus = 0");
                    rsn = stmtn.getResultSet();
                    while (rsn.next()){
                        String lctid = rsn.getString(1);
                        String lcid = rsn.getString(2);
                        String ltid = rsn.getString(3);
                        String lcul = rsn.getString(4);
                        String lyid = rsn.getString(5);
                        
                        ArrayList<String> tss1 = new ArrayList<>();
                        tss1.add(lctid);
                        for (String ts1 : tss1)
                        {
                            sbn.append(ts1);
                            sbn.append(" ");
                        }
                        ArrayList<String> tss2 = new ArrayList<>();
                        tss2.add(lcid);
                        for (String ts2 : tss2)
                        {
                            sbn1.append(ts2);
                            sbn1.append(" ");
                        }                       
                        ArrayList<String> tss3 = new ArrayList<>();
                        tss3.add(ltid);
                        for (String ts3 : tss3)
                        {
                            sbn2.append(ts3);
                            sbn2.append(" ");
                        }
                        ArrayList<String> tss4 = new ArrayList<>();
                        tss4.add(lcul);
                        for (String ts4 : tss4)
                        {
                            sbn3.append(ts4);
                            sbn3.append(" ");
                        }
                        ArrayList<String> tss5 = new ArrayList<>();
                        tss5.add(lyid);
                        for (String ts5 : tss5)
                        {
                            sbn4.append(ts5);
                            sbn4.append(" ");
                        }
                        
                    }
                    //PLACE RESULTS OF LECTURER PREFERENCES IN ARRAYS
                    String ar = sbn.toString();
                    lctidx = ar.split("\\s");//lectid
                    System.out.print(lctidx[b]);
                    
                    
                    String ar2 = sbn1.toString();
                    lcidx = ar2.split("\\s");//cuid
                    System.out.print(lcidx[b]);
                    
                    String ar3 = sbn2.toString();
                    ltidx = ar3.split("\\s");//tid
                    System.out.print(ltidx[b]);
                    
                    String ar4 = sbn3.toString();
                    lculx = ar4.split("\\s");//courselevel
                    System.out.println(lculx[b]);
                    
                    String ar5 = sbn4.toString();
                    lyidx = ar5.split("\\s");//academic year
                    System.out.println(lyidx[b]);
                    
                    if (rsn != null) {
                        try {
                            rsn.close();
                        } catch (SQLException ex) {  }
                        rsn = null;    
                    }                        
                    if (stmtn != null) {   
                        try {
                            stmtn.close();
                        } catch (SQLException ex) { }
                        stmtn = null;
                    }
                    if (conn != null) {
                        try {
                            conn.close();
                        } catch (SQLException ex) {  }
                        conn = null;
                    }        
                } catch (SQLException ex) {
                    ex.getMessage();
                }//end database access
                
                
                lculsrx=new int[lculx.length];
                for (int g=0;g<lculx.length;g++){
                    lculsrx[g] = Integer.parseInt(lculx[g]);
                }
                
                
                 for (int s=0;(s<ltidx.length) && (s<lcidx.length) && (s<lctidx.length) && (s<lyidx.length);s++){
                     
                     try {//get the number of times that the course has been scheduled so far and limit it to a max of 2
                        connn =(Connection) DriverManager.getConnection(dbURL, username, password);
                        stmtq = (Statement) connn.createStatement(); 
                        stmtq.execute("SELECT count(DISTINCT ttnegtid) FROM tt WHERE ttnegcu = '"+lcidx[s]+"';");
                        rsq = stmtq.getResultSet();
                        while (rsq.next()){
                            ttcnt2 = rsq.getInt(1);
                        }
                        
                        System.out.println("The count for timeslot "+ltidx[s]+" is "+ ttcnt2);
                        if (rsq != null) {
                        try {
                            rsq.close();
                        } catch (SQLException ex) {  }
                        rsq = null;    
                        }             
                        
                        if (stmtq != null) { 
                            try {
                                stmtq.close();
                            } catch (SQLException ex) { }
                            stmtq = null;
                        }
                        if (connn != null) {
                            try {
                                connn.close();
                            } catch (SQLException ex) {  }
                            connn = null;
                        }        
                    } catch (SQLException ex) {
                        ex.getMessage();
                    }//end database access
                    
                     
                 
                 
                 if(ttcnt2<2){
                        
                        try{                              
                            conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                            stmtr = (Statement) conn.createStatement(); 
                            stmtr.execute("SELECT lecturerpref.lectprefid, lecturerpref.tid, lecturerpref.assignstatus, courseunits.culevel "
                                    + "FROM lecturerpref,courseunits "
                                    + "WHERE lecturerpref.tid = '"+ltidx[s]+"' "
                                    + "AND courseunits.id = lecturerpref.cuid AND courseunits.culevel ="+lculsrx[s]+";");
                            rsr = stmtr.getResultSet();            
                            
                            while (rsr.next()){
                                String lpid = rsr.getString(1);//lectprefid
                                String lasst = rsr.getString(3);//assignstatus
                                
                                ArrayList<String> tss1 = new ArrayList<>();
                                tss1.add(lpid);
                                for (String ts1 : tss1)
                                {
                                    sbna.append(ts1);
                                    sbna.append(" ");
                                }
                                ArrayList<String> tss2 = new ArrayList<>();
                                tss2.add(lasst);
                                for (String ts2 : tss2)
                                {
                                    sbnb.append(ts2);
                                    sbnb.append(" ");
                                }              
                            }
                            if (rsr != null) {
                                try {
                                    rsr.close();
                                } catch (SQLException ex) {  }
                                rsr = null;    
                            }                     
                            if (stmtr != null) {  
                                try {
                                    stmtr.close();
                                } catch (SQLException ex) { }
                                stmtr = null;
                            }
                            if (conn != null) {
                                try {
                                    conn.close();
                                } catch (SQLException ex) {  }
                                conn = null;
                            }        
                        } catch (SQLException ex) {
                            ex.getMessage();
                        }//end database access
                        
                        
                        String ar = sbna.toString();
                        lpids = ar.split("\\s");//lectid
                        
                        lpidsr=new int[lpids.length];
                        for (int g=0;g<lpids.length;g++){
                            lpidsr[g] = Integer.parseInt(lpids[g]);
                        }
                        
                        
                        try{   
                            //get the room to be used
                            conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                            stmtr = (Statement) conn.createStatement();
                            stmtr.execute("SELECT DISTINCT roomkb.rid FROM roomkb, courseunits  "
                                    + "WHERE roomkb.possibleunit = courseunits.cuid and courseunits.id = '"+lcidx[s]+"' "
                                    + "AND courseunits.culevel =  "+lculsrx[s]+";");
                            rsr = stmtr.getResultSet();
                            while (rsr.next()){
                                rooom = rsr.getString(1);    
                            }
                            //System.out.println("Allocated room "+rooom+" to "+lcids[r]+" on class "+ lculsr[r]);
                            if (rsr != null) {
                                try {
                                    rsr.close();
                                } catch (SQLException ex) {  }
                                rsr = null;    
                            }                        
                            if (stmtr != null) {   
                                try {
                                    stmtr.close();
                        } catch (SQLException ex) { }
                                stmtr = null;
                            }
                            if (connn != null) {
                                try {
                                    connn.close();
                                } catch (SQLException ex) {  }
                                connn = null;
                            }        
                        } catch (SQLException ex) {
                            ex.getMessage();
                        }//end database access
                    
                       
                        
                         try{   
                        //commit all the details to the tt table 
                        conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                        stmtt = (Statement) conn.createStatement(); 
                        stmtt.execute("INSERT INTO TT (TTNegCU, TTNegLID, TTNegRID, TTNegTID, TTNegAcademicYear) "
                                + "VALUES ('"+lcidx[s]+"','"+lctidx[s]+"', '"+rooom+"', (SELECT TIMESLOTS.TID "
                                + "FROM timeslots, lecturerpref, schedule, department "
                                + "WHERE NOT lecturerpref.tid = timeslots.tid "
                                + "AND NOT lecturerpref.tid "
                                + "AND lecturerpref.cuid = '"+lcidx[s]+"' AND lecturerpref.assignstatus = '0'  "
                                + "AND schedule.schtype = timeslots.schtype  "
                                + "AND department.schtype = schedule.schtype  "
                                + "AND department.id = '"+dept+"' ORDER BY RAND() LIMIT 1), '"+lyidx[s]+"');");
                        rst = stmtt.getResultSet();  
                        if (rst != null) {
                        try {
                            rst.close();
                        } catch (SQLException ex) {  }
                        rst = null;    
                        }                 
                        
                        if (stmtt != null) {  
                            try {
                                stmtt.close();
                            } catch (SQLException ex) { }
                            stmtt = null;
                        }
                        if (conn != null) {
                            try {
                                conn.close();
                            } catch (SQLException ex) {  }
                            conn = null;
                        }        
                     } catch (SQLException ex) {
                         ex.getMessage();
                     }//end database access
                     
                     try{
                         conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                         stmtu = (Statement) conn.createStatement(); 
                         stmtu.execute("UPDATE lecturerpref SET AssignStatus='1' where lectprefid = '"+lpids[s]+"';");
                         rsu = stmtu.getResultSet();     
                         if (rsu != null) {
                             try {
                                 rsu.close();
                             } catch (SQLException ex) {  }
                             rsu = null;    
                         }               
                         
                         if (stmtu != null) {                               
                             try {
                                 stmtu.close();
                             } catch (SQLException ex) { }
                             stmtu = null;
                         }
                         if (connn != null) {
                             try {
                                 connn.close();
                             } catch (SQLException ex) {  }
                             connn = null;
                         }        
                     } catch (SQLException ex) {
                         ex.getMessage();
                     }//end database access
                     
                        System.out.println("Scheduled the  course");
                   
                    }
                
                
                
                }
                
                
            
            }
            
            
            
            /*
            for (int s=0;(s<ltids.length) && (s<lcids.length) && (s<lctids.length) && (s<lyids.length);s++){
                /*get the number of times each of the courses have been scheduled.
                 * if the courses have been scheduled less than 2 times, look for alternative slots
                */
            /*    try{
                         conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                         stmtu = (Statement) conn.createStatement(); 
                         stmtu.execute("SELECT count(distinct tt.ttnegtid), tt.ttnegtid ,tt.ttnegcu, tt.ttneglid "
                                 + "FROM tt WHERE ttnegcu = '"+lcids[s]+"';");
                         rsu = stmtu.getResultSet();    
                         while (rsu.next()){
                            //schcnt = rsu.getInt(1);
                            String tti = rsu.getString(2);//lectprefid
                            String tcu = rsu.getString(3);//cuid
                            String tli = rsu.getString(4);//assignstatus
                            
                            
                            
                            ArrayList<String> tss1 = new ArrayList<>();
                        tss1.add(tti);
                        for (String ts1 : tss1)
                        {
                            sbny1.append(ts1);
                            sbny1.append(" ");
                        }
                        ArrayList<String> tss2 = new ArrayList<>();
                        tss2.add(tcu);
                        for (String ts2 : tss2)
                        {
                            sbny2.append(ts2);
                            sbny2.append(" ");
                        }                       
                        ArrayList<String> tss3 = new ArrayList<>();
                        tss3.add(tli);
                        for (String ts3 : tss3)
                        {
                            sbny3.append(ts3);
                            sbny3.append(" ");
                        }
                        }
                         
                         
                          String ar = sbny1.toString();
                        tttid = ar.split("\\s");
                        
                        String ar2 = sbny2.toString();
                        ttcu = ar2.split("\\s");
                    
                        String ar3 = sbny3.toString();//tid
                        ttlid = ar3.split("\\s");
                         if (rsu != null) {
                             try {
                                 rsu.close();
                             } catch (SQLException ex) {  }
                             rsu = null;    
                         }               
                         
                         if (stmtu != null) {                               
                             try {
                                 stmtu.close();
                             } catch (SQLException ex) { }
                             stmtu = null;
                         }
                         if (connn != null) {
                             try {
                                 connn.close();
                             } catch (SQLException ex) {  }
                             connn = null;
                         }        
                     } catch (SQLException ex) {
                         ex.getMessage();
                     }//end database access
               
                                   
                    
                  for (int w=0; w<ttcu.length;w++) { 
                   
                        try {//get the number of times that this course has been scheduled so far and limit it to a max of 2
                        connn =(Connection) DriverManager.getConnection(dbURL, username, password);
                        stmtq = (Statement) connn.createStatement(); 
                        stmtq.execute("SELECT count(DISTINCT ttnegtid) FROM tt WHERE ttnegcu = '"+ttcu[w]+"';");
                        rsq = stmtq.getResultSet();
                        while (rsq.next()){
                            ttcnt2 = rsq.getInt(1);
                        }
                        
                        System.out.println("The couououounts are "+ ttcnt2);
                        if (rsq != null) {
                        try {
                            rsq.close();
                        } catch (SQLException ex) {  }
                        rsq = null;    
                        }             
                        
                        if (stmtq != null) { 
                            try {
                                stmtq.close();
                            } catch (SQLException ex) { }
                            stmtq = null;
                        }
                        if (connn != null) {
                            try {
                                connn.close();
                            } catch (SQLException ex) {  }
                            connn = null;
                        }        
                    } catch (SQLException ex) {
                        ex.getMessage();
                    }//end database access
                    
 
                        
                        if(ttcnt2<2){
                        try{   
                            //get the room to be used
                            conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                            stmtr = (Statement) conn.createStatement();
                            stmtr.execute("SELECT DISTINCT roomkb.rid FROM roomkb, courseunits  "
                                    + "WHERE roomkb.possibleunit = courseunits.cuid and courseunits.id = '"+lcids[s]+"' "
                                    + "AND courseunits.culevel =  "+lculsr[s]+";");
                            rsr = stmtr.getResultSet();
                            while (rsr.next()){
                                rooom = rsr.getString(1);    
                            }
                            
                            if (rsr != null) {
                                try {
                                    rsr.close();
                                } catch (SQLException ex) {  }
                                rsr = null;    
                            }                        
                            if (stmtr != null) {   
                                try {
                                    stmtr.close();
                        } catch (SQLException ex) { }
                                stmtr = null;
                            }
                            if (connn != null) {
                                try {
                                    connn.close();
                                } catch (SQLException ex) {  }
                                connn = null;
                            }        
                        } catch (SQLException ex) {
                            ex.getMessage();
                        }//end database access
                        
                         try{   
                        //commit all the details to the tt table 
                        conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                        stmtt = (Statement) conn.createStatement(); 
                        stmtt.execute("INSERT INTO TT (TTNegCU, TTNegLID, TTNegRID, TTNegTID, TTNegAcademicYear) "
                                + "VALUES ('"+ttcu[w]+"','"+tttid[w]+"', '"+rooom+"', '(SELECT TIMESLOTS.TID "
                                + "FROM timeslots, lecturerpref, schedule, department "
                                + "WHERE NOT lecturerpref.tid = timeslots.tid "
                                + "AND lecturerpref.cuid = '"+ttcu[w]+"' "
                                + "AND lecturerpref.assignstatus = '0' "
                                + "AND schedule.schtype = timeslots.schtype "
                                + "AND department.schtype = schedule.schtype "
                                + "AND department.id = '"+dept+"' "
                                + "ORDER BY RAND() LIMIT 1)' , '"+lyids[s]+"');");
                        rst = stmtt.getResultSet();  
                        if (rst != null) {
                        try {
                            rst.close();
                        } catch (SQLException ex) {  }
                        rst = null;    
                        }                 
                        
                        if (stmtt != null) {  
                            try {
                                stmtt.close();
                            } catch (SQLException ex) { }
                            stmtt = null;
                        }
                        if (conn != null) {
                            try {
                                conn.close();
                            } catch (SQLException ex) {  }
                            conn = null;
                        }        
                     } catch (SQLException ex) {
                         ex.getMessage();
                     }//end database access
                     
                     try{
                         conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                         stmtu = (Statement) conn.createStatement(); 
                         stmtu.execute("UPDATE lecturerpref SET AssignStatus='1' where lectprefid = '"+lpids[s]+"';");
                         rsu = stmtu.getResultSet();     
                         if (rsu != null) {
                             try {
                                 rsu.close();
                             } catch (SQLException ex) {  }
                             rsu = null;    
                         }               
                         
                         if (stmtu != null) {                               
                             try {
                                 stmtu.close();
                             } catch (SQLException ex) { }
                             stmtu = null;
                         }
                         if (connn != null) {
                             try {
                                 connn.close();
                             } catch (SQLException ex) {  }
                             connn = null;
                         }        
                     } catch (SQLException ex) {
                         ex.getMessage();
                     }//end database access
                     
                        System.out.println("Scheduled the  course");
                   
                    }
                        
                       
                    else{
                        System.out.println("The maximum schedule for the course has been reached");
                    }//end checking for max number of times that course is be scheduled

                        
                     }//end fetched preferences for loop
                    
                  
                    
                    
                    
                    
               
            }//end int s  for  loop
            
            
         */
                
        
            
            }
            
            myAgent.blockingReceive();
        }

        @Override
        public boolean done() {
            return false;
        }
        
    }
}
    

