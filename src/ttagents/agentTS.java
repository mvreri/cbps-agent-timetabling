package ttagents;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


/**
 *
 * @author MURERI
 */
public class agentTS extends Agent {
    
    private AID[] otherAgents;
    private String reqGot=null,tsid=null,cuid=null,
            prcuid=null,trans=null,trans2=null,trans3=null,
            culevel=null;
    private Connection conn = null;
    private Statement stmt = null;
    private ResultSet rs = null;    
    private String dbURL = "jdbc:mysql://localhost/timetabling";
    private String username = "root";
    private String password = "";
    public ttagents.agentL agl;
    
    StringBuilder sb = new StringBuilder();
    StringBuilder sb2 = new StringBuilder();
    StringBuilder sb3 = new StringBuilder();
    
    public void setup(){
        System.out.println("The agent " + getAID().getName() + " is now starting");
        
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sdTS = new ServiceDescription();
        sdTS.setType("Nego");
        sdTS.setName(getLocalName());
        dfd.addServices(sdTS);
        
        /*try {        
            DFService.register(this, dfd);
            //DFService.register(this, TSdfd);
        } catch (FIPAException ex) {
            ex.getMessage();
            ex.printStackTrace();
        }*/
        
        DFAgentDescription dfd1 = new DFAgentDescription();
        dfd1.setName(getAID());
        ServiceDescription TSsd = new ServiceDescription();
        TSsd.setType("Negot");
        TSsd.setName(getLocalName());
        dfd1.addServices(TSsd);
        
       
        try {        
            DFService.register(this, dfd1);
            //DFService.register(this, TSdfd);
        } catch (FIPAException ex) {
            ex.getMessage();
            ex.printStackTrace();
        }
        
        //addBehaviour(new agentTS.getOrders());
        addBehaviour(new agentTS.getSch());//to deal with requests from agentL
             
        //addBehaviour(new agentTS.sendSch());//respond to the requests by agentL
        //addBehaviour(new agentTS.sendCUID());
        //addBehaviour(new agentTS.negPartA());
    }//end setup
    
    
    public void takeDown(){
        try {
            DFService.deregister(this);
        } catch (FIPAException ex) {
           ex.getMessage();
           ex.printStackTrace();
        }
    }//end takedown
    
    
    private class getSch extends CyclicBehaviour{
        
        private String reqGot;

        @Override
        public void action() {
            reset();
            MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.CFP);
            ACLMessage req = myAgent.receive(mt);
            
            if (req != null){
                reqGot = req.getContent();
                System.out.println("AgentTS: Get sch; received " + reqGot + " from " + req.getSender().getLocalName());//test
                ACLMessage reply = req.createReply();
                
                //convert received string to an array
                String[] lids= reqGot.split("\\s");
                for (int x=0; x<lids.length; x++)
                    System.out.println(lids[x]);
                
                try {
                        Class.forName("com.mysql.jdbc.Driver");
                    } catch (ClassNotFoundException ex) {
                        ex.getMessage();
                        ex.printStackTrace();
                    }
                    try {
                        conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                        stmt = (Statement) conn.createStatement();
                        /*get the timeslot duration and number for a given class group in a given school
                         * avail these to the negotiation to guide in allocation by lecturers
                         * Parse the timeslot IDs to reveal the different class groups' */
                        for(int i=0; i<lids.length;i++){
                        if (stmt.execute("SELECT lecturerpref.TID "
                                + "FROM timeslots, department, Schedule, lecturers, lecturerpref, courseunits "
                                + "WHERE lecturers.LID = '"+lids[i]+"'  AND lecturerpref.lectid = lecturers.ID "
                                + "AND lecturerpref.tid = timeslots.tid AND lecturers.DID = department.id AND courseunits.id = lecturerpref.cuid "
                                + "AND department.Schtype=Schedule.Schtype and timeslots.schtype = schedule.schtype;")) {
                            rs = stmt.getResultSet();
                        } else {                  
                            System.err.println("Select Failed");
                        }
                        
                        while (rs.next()) {                        
                            String entry1 = rs.getString(1);//get the timeslot id booked     
                            //String entry2 = rs.getString(16);//course unit cuid
                            //String entry3 = rs.getString(19);//course unit cuid from lectpref

                            tsid = entry1;
                            //cuid = entry2;
                            //prcuid = entry3;
                            //System.out.println(tsid);
                            //send the timeslot and the units booked for that slot
                            ArrayList<String> list = new ArrayList<String>();
                            list.add(tsid);
                            for (String s : list)
                                {
                                    sb.append(s);
                                    sb.append(" ");
                                }
                            
                            ArrayList<String> list2 = new ArrayList<String>();
                            list2.add(cuid);
                            for (String s2 : list2)
                                {
                                    sb2.append(s2);
                                    sb2.append(" ");
                                }
                            
                            ArrayList<String> list3 = new ArrayList<String>();
                            list3.add(prcuid);
                            for (String s3 : list3)
                                {
                                    sb3.append(s3);
                                    sb3.append(" ");
                                }
                            
                        }
                        
                        }  
                        trans = sb.toString();
                                               
                        
                        System.out.println("AgentTS: Timeslots' units " +trans);
                        
                        
                        
                        if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException ex) { /* ignore */ }
                    rs = null;    
                }
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException ex) { /* ignore */ }
                    stmt = null;
                }
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException ex) { /* ignore */ }
                    conn = null;
                }          
                         
                        
                    } catch (SQLException ex) {
                        ex.getMessage();
                    }//end database access

                if(reqGot != null){
                    
                    reply.setPerformative(ACLMessage.PROPOSE);
                    reply.setContent(trans);
                    System.out.println("sent " + trans );
                }
                else{
                    reply.setPerformative(ACLMessage.REFUSE);
                    reply.setContent("parameter-unavailable");
                }                        
                myAgent.send(reply);
                myAgent.blockingReceive();
            }
            else{
                block();
            }
        }
        
    }//end getSch
    
    
    private class sendSch extends CyclicBehaviour{

        @Override
        public void action() {
            MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.AGREE);         
            ACLMessage req = myAgent.receive(mt);     
            
            
            
            if (req != null){
                reqGot = req.getContent();                
                ACLMessage reply = req.createReply();
                
                
                //convert received string to an array
                String[] lids= reqGot.split("\\s");
                for (int x=0; x<lids.length; x++)
                    System.out.println(lids[x]);
                
                try {
                        Class.forName("com.mysql.jdbc.Driver");
                    } catch (ClassNotFoundException ex) {
                        ex.getMessage();
                        ex.printStackTrace();
                    }
                    try {
                        conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                        stmt = (Statement) conn.createStatement();
                        /*get the timeslot duration and number for a given class group in a given school
                         * avail these to the negotiation to guide in allocation by lecturers
                         * Parse the timeslot IDs to reveal the different class groups' */
                        for(int i=0; i<lids.length;i++){
                        if (stmt.execute("SELECT timeslots.id, timeslots.tid, timeslots.Schtype, timeslots.Tstart, timeslots.Tstop,  department.DID,"
                                + " department.Schtype, department.ID, lecturers.lid, lecturers.lsname, lecturerpref.lactid, lecturerpref.prefdate,"
                                + " lectureract.lactid, lectureract.lid,courseunits.id, courseunits.cuid, courseunits.cuname, courseunits.culevel,lecturerpref.cuid "
                                + "FROM timeslots, department, Schedule, lecturers, lectureract, lecturerpref, courseunits "
                                + "WHERE lecturers.LID = '"+lids[i]+"'  AND lectureract.lid = lecturers.LID AND lectureract.lactid = lecturerpref.lactid"
                                + " AND lecturerpref.tid = timeslots.tid AND lecturers.DID = department.did AND courseunits.id = lecturerpref.cuid "
                                + "AND department.Schtype=Schedule.Schtype and timeslots.schtype = schedule.schtype;")) {
                            rs = stmt.getResultSet();
                        } else {                  
                            System.err.println("Select Failed");
                        }
                        
                        while (rs.next()) {                        
                                
                            String entry2 = rs.getString(16);//course unit cuid
                            cuid = entry2;
                         
                            //System.out.println(tsid);
                            //send the timeslot and the units booked for that slot
                           
                            ArrayList<String> list2 = new ArrayList<String>();
                            list2.add(cuid);
                            for (String s2 : list2)
                                {
                                    sb2.append(s2);
                                    sb2.append(" ");
                                }
                           
                        }
                        
                        }  
                       
                        trans2 = sb2.toString();
                        System.out.println("AgentTS: Timeslots " +trans2);
                       
                        if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException ex) { /* ignore */ }
                    rs = null;    
                }
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException ex) { /* ignore */ }
                    stmt = null;
                }
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException ex) { /* ignore */ }
                    conn = null;
                }          
                         
                        
                    } catch (SQLException ex) {
                        ex.getMessage();
                        ex.printStackTrace();
                    }//end database access
                
                
                
                
                
                if (reply != null){
                    System.out.println("AgentTS: Send Orders; Successfully gotten "+ reqGot + " from " + req.getSender().getLocalName());
                    
                    //System.out.println("AgentTS: Timeslots' ids " +trans2);                    
                    reply.setPerformative(ACLMessage.PROPOSE);
                    reply.setContent(trans2);
                    
                    System.out.println(trans2+" sent to " + req.getSender().getName() + " from" + getAID().getName());
                }
                else{
                    reply.setPerformative(ACLMessage.REFUSE);
                    reply.setContent("no-deal");
                }
                myAgent.send(reply);
                //myAgent.doDelete();
            }
            else{
                block();
            }
        }
        
    }//end sendSch 
    
    
     private class sendCUID extends CyclicBehaviour{

        
        @Override
        public void action() {
            MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.ACCEPT_PROPOSAL);         
            ACLMessage req = myAgent.receive(mt);        
            
            if (req != null){
                reqGot = req.getContent();                
                ACLMessage reply = req.createReply();
                
                
                //convert received string to an array
                String[] lids= reqGot.split("\\s");
                for (int x=0; x<lids.length; x++)
                    System.out.println(lids[x]);
                
                try {
                        Class.forName("com.mysql.jdbc.Driver");
                    } catch (ClassNotFoundException ex) {
                        ex.getMessage();
                        ex.printStackTrace();
                    }
                    try {
                        conn =(Connection) DriverManager.getConnection(dbURL, username, password);
                        stmt = (Statement) conn.createStatement();
                        /*get the timeslot duration and number for a given class group in a given school
                         * avail these to the negotiation to guide in allocation by lecturers
                         * Parse the timeslot IDs to reveal the different class groups' */
                        for(int i=0; i<lids.length;i++){
                        if (stmt.execute("SELECT timeslots.id, timeslots.tid, timeslots.Schtype, timeslots.Tstart, timeslots.Tstop,  department.DID,"
                                + " department.Schtype, department.ID, lecturers.lid, lecturers.lsname, lecturerpref.lactid, lecturerpref.prefdate,"
                                + " lectureract.lactid, lectureract.lid,courseunits.id, courseunits.cuid, courseunits.cuname, courseunits.culevel,lecturerpref.cuid "
                                + "FROM timeslots, department, Schedule, lecturers, lectureract, lecturerpref, courseunits "
                                + "WHERE lecturers.LID = '"+lids[i]+"'  AND lectureract.lid = lecturers.LID AND lectureract.lactid = lecturerpref.lactid"
                                + " AND lecturerpref.tid = timeslots.tid AND lecturers.DID = department.did AND courseunits.id = lecturerpref.cuid "
                                + "AND department.Schtype=Schedule.Schtype and timeslots.schtype = schedule.schtype;")) {
                            rs = stmt.getResultSet();
                        } else {                  
                            System.err.println("Select Failed");
                        }
                        
                        while (rs.next()) {                        
                            
                            String entry3 = rs.getString(19);//course unit cuid from lectpref

                            
                            prcuid = entry3;
                            //System.out.println(tsid);
                            //send the timeslot and the units booked for that slot
                            
                            ArrayList<String> list3 = new ArrayList<String>();
                            list3.add(prcuid);
                            for (String s3 : list3)
                                {
                                    sb3.append(s3);
                                    sb3.append(" ");
                                }
                            
                        }
                        }
                        
                       
                        trans3 = sb3.toString();                  
                        System.out.println("AgentTS: Timeslots  CUID " +trans3);
                        
                        
                        if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException ex) { /* ignore */ }
                    rs = null;    
                }
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException ex) { /* ignore */ }
                    stmt = null;
                }
                if (conn != null) {
                    try {
                        conn.close();
                    } catch (SQLException ex) { /* ignore */ }
                    conn = null;
                }          
                         
                        
                    } catch (SQLException ex) {
                        ex.getMessage();
                        ex.printStackTrace();
                    }//end database access
                
                if (reply != null){
                    System.out.println("AgentTS: Send CUID; Successfully gotten "+ reqGot + " from " + req.getSender().getLocalName());
                    
                    //System.out.println("AgentTS: Timeslots' ids " +trans3);                    
                    reply.setPerformative(ACLMessage.CONFIRM);
                    reply.setContent(trans3);
                    System.out.println(trans3+" sent to " + req.getSender().getName() + " from" + getAID().getName());
                    
                }
                else{
                    reply.setPerformative(ACLMessage.DISCONFIRM);
                    reply.setContent("no-deal");
                }
                myAgent.send(reply);
                reqGot=null;tsid=null;cuid=null;prcuid=null;trans=null;trans2=null;trans3=null;culevel=null;
                //myAgent.doDelete();
            }
            else{
                block();
            }
        }//end action
        
    } //end sendCUID
    
    
    
}
